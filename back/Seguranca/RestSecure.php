﻿<?php

if (!isset(getallheaders()['Token']) || empty(getallheaders()['Token']) || !JWT::tokenValido(getallheaders()['Token'])) {
    http_response_code(400);
    die("Token inválido.");
}