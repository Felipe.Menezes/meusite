<?php

define("PRIVATE_KEY", "evoxx_private_key_247");
define("ALGORITHM", "HS256");

require "../../vendor/autoload.php";

class JWT
{
    private $jwt;

    public function __construct($data = array())
    {
        $this->jwt = new JOSE_JWT($data);
        $this->jwt = $this->jwt->sign(PRIVATE_KEY, ALGORITHM);
    }

    public function toString()
    {
        return $this->jwt->toString();
    }

    public static function validate($token)
    {
        $decoded = JOSE_JWT::decode($token);
        $decoded->verify(PRIVATE_KEY, ALGORITHM);
    }

    public static function tokenValido($token)
    {
        if (empty($token))
            return false;

        try {
            self::validate($token);
        } catch (Exception $e) {
            return false;
        }

        $baseDecoded = base64_decode(explode(".", $token)[1]);

        $dataObj = json_decode($baseDecoded);
        $dataToken = new DateTime($dataObj->data);
        $tomorrow = new DateTime();
        $tomorrow->add(new DateInterval("P1D"));

        return ($dataToken < $tomorrow);
    }

}