<?php

header("Content-type: application/json;charset=utf-8;");

require "../../utils/evoxx_autoload.php";
require "../../Seguranca/RestSecure.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app = new Slim\App($settingsApp);

$app->get("/", 'RecuperarUsuarios');
$app->get("/{id}", 'GetUser');

function RecuperarUsuarios(Request $request, Response $response)
{

    $sql = "SELECT id,
                   nome_completo,
                   email,
                   login
              FROM usuarios
             WHERE ativo = true";

    $st = Conexao::getConnection()->prepare($sql);
    $st->execute();
    $allUsers = $st->fetchAll(PDO::FETCH_ASSOC);

    $response->getBody()->write(json_encode($allUsers));

    return $response;
}

function GetUser(Request $request, Response $response, $args)
{
     $sql = "SELECT id,
                   nome_completo,
                   email,
                   login
              FROM usuarios
             WHERE id = :id
               AND ativo = true";

    try{
        $st = Conexao::getConnection()->prepare($sql);
        $st->execute(array("id" => $args['id']));
        $user = $st->fetch(PDO::FETCH_ASSOC);
    } catch(PDOException $e){
        var_dump("Erro MYSQL: ".$e->getMessage());
    }

    if(!$user){
        http_response_code(400);
        die(json_encode(array(
            "status" => 400,
            "response" => "User not found."
        )));
    }

    $response->getBody()->write(json_encode($user));

    return $response;
}

$app->run();
