﻿<?php

error_reporting(E_ERROR);

header("Content-type: application/json;charset=UTF-8;");

include "../../utils/evoxx_autoload.php";
require "../../Seguranca/RestSecure.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App($settingsApp);

$app->get("/", 'get');
$app->put("/", 'put');

function get(Request $request, Response $response) {
  $sql = "SELECT conheca_nossos_produtos
            FROM configuracoes_site
           LIMIT 1";

  try{

    $st = Conexao::getConnection()->prepare($sql);
    $st->execute();
    return json_encode($st->fetch(PDO::FETCH_ASSOC));

  } catch (PDOException $e) {
    http_response_code(500);
    die(json_encode(array(
        "status" => 500,
        "response" => $e->getMessage()
    )));
  }
}

function put(Request $request, Response $response){
  $sql =  "UPDATE configuracoes_site
              SET conheca_nossos_produtos = :conheca_nossos_produtos";

  $requestParams = json_decode($request->getBody());

  $conhecaNossosProdutos = $requestParams->conheca_nossos_produtos;

  try {
    $st = Conexao::getConnection()->prepare($sql);
    $st->execute(array("conheca_nossos_produtos" => $conhecaNossosProdutos));
  } catch(PDOException $e) {
    http_response_code(500);
    die(json_encode(array(
        "status" => 500,
        "response" => $e->getMessage()
    )));
  }

  $resposta = array(
    "status" => 200,
    "response" => "ok"
  );

  $response->getBody()->write(json_encode($resposta));

  return $response;
}

$app->run();
