<?php

header("Content-type: application/json;charset=utf-8;");

require "../../utils/evoxx_autoload.php";
require "../../Seguranca/RestSecure.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app = new Slim\App($settingsApp);

$app->get('/', 'getAll');
$app->get('/{id:[0-9]+}', 'getCategoria');
$app->get('/WithoutPagination[/]', 'getAllWithoutPagination');
$app->post('/', 'adicionarCategoria');
$app->put('/{id}', 'putCategoria');
$app->delete('/{id}', 'deleteCategoria');


function putCategoria(Request $request, Response $response, $args)
{
    if (!isset($args['id']) || empty($args['id']) || !is_numeric($args['id'])) return erro("Id inválido!");

    $params = json_decode($request->getBody());

    if (!isset($params->descricao) || empty($params->descricao)) return erro($response, "Descrição obrigatória");

    $sql = "UPDATE produtos_categorias
               SET descricao = :descricao
             WHERE id = :id
               AND ativo = true";

    try {
        $st = Conexao::getConnection()->prepare($sql);
        $st->bindValue('descricao', $params->descricao);
        $st->bindValue('id', $args['id']);
        $alterou = $st->execute();
    } catch (PDOException $e) {
        ResponseHTTP::error("Não foi possível alterar o registro!");
    }

    $resposta = array(
        'status' => 200,
        'response' => "Alterado com sucesso!"
    );

    $response->getBody()->write(json_encode($resposta));
    return $response;
}

function deleteCategoria(Request $request, Response $response, $args)
{
    if (!isset($args['id']) || empty($args['id']) || !is_numeric($args['id'])) return erro("Id inválido!");

    $sql = "UPDATE produtos_categorias
               SET ativo = false
             WHERE id = :id
               AND ativo = true";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("id", $args['id']);
    $result = $st->execute();

    $response->getBody()->write(json_encode(array(
        'response' => "Apagado com sucesso!"
    )));

    return $response;
}

function getCategoria(Request $request, Response $response, $args)
{
    if (!isset($args['id']) || empty($args['id']) || !is_numeric($args['id'])) return erro("Id inválido!");

    $sql = "SELECT id,
                  descricao
             FROM produtos_categorias
            WHERE ativo = true
              AND id = :id";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue('id', $args['id']);
    $st->execute();

    $resultado = $st->fetchAll(PDO::FETCH_ASSOC);

    if (count($resultado) > 0) {

        $retorno = json_encode($resultado[0]);
        $response->getBody()->write($retorno);
    } else {
        http_response_code(401);
        $retorno = json_encode(array(
            "status" => 401,
            "Categoria inexistente!"
        ));
        die($retorno);
    }

    return $response;
}

function getAll(Request $request, Response $response)
{
    $params = (object)$request->getQueryParams();

    $limit = $params->limit ?: 10;
    $page = $params->page ?: 1;
    $offset = ($page - 1) * $limit;

    $sqlTotalRows = "SELECT count(id) AS 'total_rows'
                       FROM produtos_categorias
                      WHERE ativo = TRUE";

    $stTotal = Conexao::getConnection()->prepare($sqlTotalRows);
    $stTotal->execute();

    $totalRows = $stTotal->fetch(PDO::FETCH_ASSOC);
    $totalRows = $totalRows['total_rows'] ?: 0;

    $numberOfPages = (int)ceil($totalRows / $limit);

    $sql = "SELECT id,
                   descricao
              FROM produtos_categorias
             WHERE ativo = true
             LIMIT $offset, $limit";


    $stResultado = Conexao::getConnection()->prepare($sql);
    $stResultado->execute();
    $resultado = $stResultado->fetchAll(PDO::FETCH_ASSOC);

    $data = array();

    $retorno = array(
        'data' => $resultado,
        'numberOfPages' => $numberOfPages,
        'totalRows' => $totalRows
    );

    $response->getBody()->write(json_encode($retorno));

    return $response;
}

function adicionarCategoria(Request $request, Response $response)
{
    $params = json_decode($request->getBody());

    if (!isset($params->descricao) || empty($params->descricao)) return erro($response, "Descrição obrigatória");


    $sql = "INSERT INTO produtos_categorias
                    SET descricao = :descricao";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("descricao", $params->descricao);

    $st->execute();

    $resposta = array(
        "status" => 200,
        "response" => "ok"
    );

    $response->getBody()->write(json_encode($resposta));

    return $response;

}

function erro($response, $mensagem)
{
    $response->getBody()->write(json_encode(array(
        "status" => 401,
        "response" => $mensagem
    )));

    return false;
}

function getAllWithoutPagination(Request $request, Response $response)
{

    $sql = "SELECT id,
                   descricao
              FROM produtos_categorias
             WHERE ativo = TRUE
          ORDER BY descricao, id";

    try {
        $st = Conexao::getConnection()->prepare($sql);
        $st->execute();
        $resultado = $st->fetchAll(PDO::FETCH_ASSOC);

        $response->getBody()->write(json_encode($resultado));
    } catch (PDOException $e) {
        ResponseHTTP::error("Erro interno", $e->getMessage());
    }

    return $response;
}

$app->run();

?>