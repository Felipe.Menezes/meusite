<?php

header("Content-type: application/json;charset=utf-8;");

require "../../utils/evoxx_autoload.php";
require "../../Seguranca/RestSecure.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app = new Slim\App($settingsApp);

$app->get('/', 'getAll');
$app->get('/{id:[0-9]+}', 'getProduto');
$app->get('/{idProduto:[0-9]+}/Caracteristicas', 'getCaracteristicas');
$app->get('/{idProduto:[0-9]+}/Especificacoes', 'getEspecificacoes');

$app->post('/', 'adicionarProduto');
$app->post('/{idProduto:[0-9]+}/AdicionarFoto[/]', 'adicionarFoto');
$app->post('/{idProduto:[0-9]+}/SetManual[/]', 'setManual');
$app->post('/{idProduto:[0-9]+}/AdicionarCaracteristica[/]', 'adicionarCaracteristica');
$app->post('/{idProduto:[0-9]+}/AdicionarEspecificacao[/]', 'adicionarEspecificacao');

$app->put('/{id:[0-9]+}', 'putNoticia');

$app->delete('/{id:[0-9]+}', 'deleteProduto');
$app->delete('/{idFoto:[0-9]+}/RemoverFoto[/]', 'removerFoto');
$app->delete('/{idProduto:[0-9]+}/ExcluirCaracteristica/{id:[0-9]+}', 'excluirCaracteristica');
$app->delete('/{idProduto:[0-9]+}/ExcluirEspecificacao/{id:[0-9]+}', 'excluirEspecificacao');

define("DIRECTORY_IMAGES_PRODUTOS", DIRECTORY_IMAGES . "produtos/");
define("DIRECTORY_FILES_PRODUTOS", DIRECTORY_FILES . "produtos/");
define("URL_IMAGES", PUBLIC_URL . "images/produtos/");
define("URL_FILES", PUBLIC_URL . "files/produtos/");

function getEspecificacoes(Request $request, Response $response, $args)
{
    $sql = "SELECT id,
                   titulo,
                   valor
              FROM produtos_especificacoes
             WHERE id_produto = :id_produto
               AND ativo = TRUE";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("id_produto", $args['idProduto']);
    $st->execute();

    $fetch = $st->fetchAll(PDO::FETCH_ASSOC);

    return $response->withJson($fetch);
}

function getCaracteristicas(Request $request, Response $response, $args)
{
    $sql = "SELECT produtos_caracteristicas.id,
                   produtos_caracteristicas.id_tipo,
                   produtos_caracteristicas.caracteristica,
                   produtos_tipo_caracteristicas.descricao as tipo
              FROM produtos_caracteristicas
         LEFT JOIN produtos_tipo_caracteristicas
                ON produtos_tipo_caracteristicas.id = produtos_caracteristicas.id_tipo
             WHERE produtos_caracteristicas.id_produto = :id_produto
               AND produtos_tipo_caracteristicas.ativo = TRUE
               AND produtos_caracteristicas.ativo = TRUE";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("id_produto", $args['idProduto']);
    $st->execute();

    $fetch = $st->fetchAll(PDO::FETCH_ASSOC);

    return $response->withJson($fetch);
}

function adicionarCaracteristica(Request $request, Response $response, $args)
{

    if (!isset($args['idProduto']) || empty($args['idProduto']) || !is_numeric($args['idProduto'])) ResponseHTTP::error("Id inválido!");

    $params = (object)$request->getParsedBody();

    if (!isset($params->id_tipo) || empty($params->id_tipo)) ResponseHTTP::error("Tipo é obrigatório", "", 401);
    if (!isset($params->caracteristica) || empty($params->caracteristica)) ResponseHTTP::error("Característica é obrigatório", "", 401);

    $sql = "INSERT INTO produtos_caracteristicas
                    SET id_tipo = :id_tipo,
                        id_produto = :id_produto,
                        caracteristica = :caracteristica";

    $db = Conexao::getConnection();

    $st = $db->prepare($sql);
    $st->bindValue("id_tipo", $params->id_tipo);
    $st->bindValue("id_produto", $args['idProduto']);
    $st->bindValue("caracteristica", $params->caracteristica);
    $st->execute();

    return $response->withJson(array(
        "status" => 200,
        "response" => "ok",
        "id" => $db->lastInsertId()
    ));
}

function adicionarEspecificacao(Request $request, Response $response, $args)
{
    if (!isset($args['idProduto']) || empty($args['idProduto']) || !is_numeric($args['idProduto'])) ResponseHTTP::error("Id inválido!");

    $params = (object)$request->getParsedBody();

    if (!isset($params->titulo) || empty($params->titulo)) ResponseHTTP::error("Título é obrigatório", "", 401);
    if (!isset($params->valor) || empty($params->valor)) ResponseHTTP::error("Valor é obrigatório", "", 401);

    $sql = "INSERT INTO produtos_especificacoes
                    SET id_produto = :id_produto,
                        titulo = :titulo,
                        valor = :valor";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("id_produto", $args['idProduto']);
    $st->bindValue("titulo", $params->titulo);
    $st->bindValue("valor", $params->valor);
    $st->execute();

    return $response->withJson(array(
        "status" => 200,
        "response" => "ok"
    ));
}

function excluirCaracteristica(Request $request, Response $response, $args)
{
    if (!isset($args['id']) || empty($args['id']) || !is_numeric($args['id'])) ResponseHTTP::error("Id inválido!");

    $sql = "DELETE FROM produtos_caracteristicas
                  WHERE id = :id";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("id", $args['id']);
    $st->execute();

    return $response->withJson(array(
        "status" => 200,
        "response" => "ok"
    ));
}

function excluirEspecificacao(Request $request, Response $response, $args)
{
    if (!isset($args['id']) || empty($args['id']) || !is_numeric($args['id'])) ResponseHTTP::error("Id inválido!");

    $sql = "DELETE FROM produtos_especificacoes
                  WHERE id = :id";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("id", $args['id']);
    $st->execute();

    return $response->withJson(array(
        "status" => 200,
        "response" => "ok"
    ));
}

function setManual(Request $request, Response $response, $args)
{
    $files = $request->getUploadedFiles();

    $sql = "UPDATE produtos
               SET manual = :manual
             WHERE id = :id";

    $uploadedFile = $files['manual'];
    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
        $fileName = moveUploadedFile(DIRECTORY_FILES_PRODUTOS, $uploadedFile);

        $st = Conexao::getConnection()->prepare($sql);
        $st->bindValue("id", $args['idProduto']);
        $st->bindValue("manual", $fileName);
        $st->execute();
    }

    return $response->withJson(array(
        "status" => 200,
        "response" => "Salvo com sucesso"
    ));
}

function adicionarFoto(Request $request, Response $response, $args)
{
    $files = $request->getUploadedFiles();

    $sql = "INSERT INTO produtos_fotos
                    SET id_produto = :id_produto,
                        foto = :foto";

    foreach ($files['imagens'] as $uploadedFile) {
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = moveUploadedFile(DIRECTORY_IMAGES_PRODUTOS, $uploadedFile);

            $st = Conexao::getConnection()->prepare($sql);
            $st->bindValue("id_produto", $args['idProduto']);
            $st->bindValue("foto", $filename);
            $st->execute();
        }
    }

    $resposta = array(
        'status' => 200,
        'response' => "Cadastrado com sucesso!"
    );

    return $response->withJson($resposta);
}

function removerFoto(Request $request, Response $response, $args)
{
    $nomeFoto = "";

    $sqlSelect = "SELECT foto 
                    FROM produtos_fotos 
                   WHERE id = :id";

    $stSelect = Conexao::getConnection()->prepare($sqlSelect);
    $stSelect->bindValue("id", $args['idFoto']);
    $stSelect->execute();

    $fetch = $stSelect->fetch(PDO::FETCH_ASSOC);
    $nomeFoto = $fetch['foto'];

    echo unlink(DIRECTORY_IMAGES_NOTICIAS . $nomeFoto);

    $sql = "DELETE FROM produtos_fotos
                  WHERE id = :id";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("id", $args['idFoto']);
    $st->execute();

    return $response->withJson(array(
        'response' => "Apagado com sucesso!"
    ));
}

function putNoticia(Request $request, Response $response, $args)
{
    if (!isset($args['id']) || empty($args['id']) || !is_numeric($args['id'])) ResponseHTTP::error("Id inválido!");

    $params = (object)$request->getParsedBody();

    $intValCategoria = intval($params->id_categoria);
    if (!isset($params->id_categoria) || empty($params->id_categoria) || $intValCategoria === 0) ResponseHTTP::error("Categoria é obrigatória", "", 401);
    if (!isset($params->descricao) || empty($params->descricao)) ResponseHTTP::error("Descrição é obrigatória", "", 401);

    $sql = "UPDATE produtos
               SET id_categoria = :id_categoria,
                   descricao = :descricao,
                   subdescricao = :subdescricao
             WHERE id = :id
               AND ativo = TRUE";

    try {
        $st = Conexao::getConnection()->prepare($sql);
        $st->bindValue("id_categoria", $intValCategoria);
        $st->bindValue("descricao", $params->descricao);
        $st->bindValue("subdescricao", $params->subdescricao);
        $st->bindValue("id", $args['id']);
        $st->execute();

    } catch (PDOException $e) {
        ResponseHTTP::error("Erro ao alterar o produto", $e->getMessage());
    }

    return $response->withJson(array(
        "status" => 200,
        "response" => "Alterado com sucesso!"
    ));
}

function deleteProduto(Request $request, Response $response, $args)
{
    if (!isset($args['id']) || empty($args['id']) || !is_numeric($args['id'])) return ResponseHTTP::error("Id inválido!");

    $sql = "UPDATE produtos
               SET ativo = false
             WHERE id = :id
               AND ativo = true";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("id", $args['id']);
    $result = $st->execute();

    $response->getBody()->write(json_encode(array(
        'response' => "Apagado com sucesso!"
    )));

    return $response;
}

function getProduto(Request $request, Response $response, $args)
{
    if (!isset($args['id']) || empty($args['id']) || !is_numeric($args['id'])) ResponseHTTP::error("Id inválido!");

    $sql = "SELECT produtos.id,
                   produtos.id_categoria,
                   produtos.descricao,
                   produtos.subdescricao,
                   produtos.manual
              FROM produtos
             WHERE produtos.ativo = true
               AND produtos.id = :id
             LIMIT 1";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue('id', $args['id']);
    $st->execute();

    $resultado = $st->fetch(PDO::FETCH_ASSOC);

    if ($resultado) {
        $resultado['fotos'] = array();

        $sqlFotos = "SELECT id,
                            foto
                       FROM produtos_fotos
                      WHERE id_produto = :id
                        AND ativo = TRUE
                   ORDER BY id";

        $stFotos = Conexao::getConnection()->prepare($sqlFotos);
        $stFotos->bindValue('id', $resultado['id']);
        $stFotos->execute();
        $fotos = $stFotos->fetchAll(PDO::FETCH_ASSOC);

        foreach ($fotos as $ft) {
            $ft['foto'] = URL_IMAGES . $ft['foto'];
            array_push($resultado['fotos'], $ft);
        }
    }


    if ($resultado)
        return $response->withJson($resultado);
    else
        ResponseHTTP::error("Produto inexistente!");

    return $response;
}

function getAll(Request $request, Response $response)
{
    $params = (object)$request->getQueryParams();

    $limit = $params->limit ?: 10;
    $page = $params->page ?: 1;
    $offset = ($page - 1) * $limit;

    $sqlTotalRows = "SELECT count(id) AS 'total_rows'
                       FROM produtos
                      WHERE produtos.ativo = TRUE";

    $stTotal = Conexao::getConnection()->prepare($sqlTotalRows);
    $stTotal->execute();

    $totalRows = $stTotal->fetch(PDO::FETCH_ASSOC);
    $totalRows = $totalRows['total_rows'] ?: 0;

    $numberOfPages = (int)ceil($totalRows / $limit);

    $sql = "SELECT produtos.id,
                   produtos.id_categoria,
                   produtos.descricao,
                   produtos_categorias.descricao as categoria
              FROM produtos
         LEFT JOIN produtos_categorias
                ON produtos_categorias.id = produtos.id_categoria
             WHERE produtos.ativo = true
               AND produtos_categorias.ativo = TRUE
          ORDER BY produtos.descricao
             LIMIT $offset, $limit";


    $stResultado = Conexao::getConnection()->prepare($sql);
    $stResultado->execute();
    $resultado = $stResultado->fetchAll(PDO::FETCH_ASSOC);

    $retorno = array(
        'data' => $resultado,
        'numberOfPages' => $numberOfPages,
        'totalRows' => $totalRows
    );

    $response->getBody()->write(json_encode($retorno));

    return $response;

}

function adicionarProduto(Request $request, Response $response)
{
    $params = (object)$request->getParsedBody();
    $files = $request->getUploadedFiles();

    $intValCategoria = intval($params->id_categoria);
    if (!isset($params->id_categoria) || empty($params->id_categoria) || $intValCategoria === 0) ResponseHTTP::error("Categoria é obrigatória", "", 401);
    if (!isset($params->descricao) || empty($params->descricao)) ResponseHTTP::error("Descrição é obrigatória", "", 401);

    $sql = "INSERT INTO produtos
                    SET id_categoria = :id_categoria,
                        descricao = :descricao,
                        subdescricao = :subdescricao";

    $fileNameManual = "";
    if ($files['manual']) {
        $sql .= " ,manual = :manual";
        $fileNameManual = moveUploadedFile(DIRECTORY_FILES_PRODUTOS, $files['manual']);
    }

    $db = Conexao::getConnection();
    $st = $db->prepare($sql);
    $st->bindValue("id_categoria", $intValCategoria);
    $st->bindValue("descricao", $params->descricao);
    $st->bindValue("subdescricao", $params->subdescricao);

    if ($files['manual'])
        $st->bindValue("manual", $fileNameManual);

    $st->execute();

    $idProduto = $db->lastInsertId();

    if (count($files['imagens'])) {
        $sqlFotos = "INSERT INTO produtos_fotos
                             SET foto = :foto,
                                 id_produto = :id_produto";

        foreach ($files['imagens'] as $foto) {

            if ($foto->getError() === UPLOAD_ERR_OK) {
                $fileName = moveUploadedFile(DIRECTORY_IMAGES_PRODUTOS, $foto);

                $stFotos = $db->prepare($sqlFotos);
                $stFotos->bindValue("foto", $fileName);
                $stFotos->bindValue("id_produto", $idProduto);
                $stFotos->execute();
            }
        }
    }

    return $response->withJson(array(
        "status" => 200,
        "response" => "Salvo com sucesso!"
    ));
}

$app->run();

?>