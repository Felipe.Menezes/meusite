<?php

header("Content-type: application/json;charset=UTF-8;");

include "../../utils/evoxx_autoload.php";
require "../../Seguranca/RestSecure.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App($settingsApp);

$app->get("/", 'get');
$app->put("/", 'put');

function get(Request $request, Response $response) {
  $sql = "SELECT titulo,
                 artigo
            FROM institucional_quem_somos
        ORDER BY id DESC
           LIMIT 1";

  try{

    $st = Conexao::getConnection()->prepare($sql);
    $st->execute();
    return json_encode($st->fetch(PDO::FETCH_ASSOC));

  } catch (PDOException $e) {
    http_response_code(500);
    die(json_encode(array(
        "status" => 500,
        "response" => $e->getMessage()
    )));
  }
}

function putValid($requestParams){
    if(!isset($requestParams->titulo) || empty($requestParams->titulo))
        return ResponseHTTP::error("Informe um título");
    if(!isset($requestParams->artigo) || empty($requestParams->artigo))
        return ResponseHTTP::error("Informe um artigo");

    return true;
}

function put(Request $request, Response $response){
  $sql =  "UPDATE institucional_quem_somos
              SET titulo = :titulo,
                  artigo = :artigo";

  $requestParams = json_decode($request->getBody());

  if(!putValid($requestParams))
    return;

  try {
    
    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue('titulo', $requestParams->titulo);
    $st->bindValue('artigo', $requestParams->artigo);
    $st->execute();

  } catch(PDOException $e) {
      ResponseHTTP::error("Erro na consulta", $e->getMessage(), 500);
  }

  $resposta = array(
    "status" => 200,
    "response" => "ok"
  );

  $response->getBody()->write(json_encode($resposta));

  return $response;
}

$app->run();
