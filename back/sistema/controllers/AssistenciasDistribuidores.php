<?php

include "../../utils/evoxx_autoload.php";
include "../../utils/GeoController.php";
require "../../Seguranca/RestSecure.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App($settingsApp);

$app->get("[/]", 'getAll');
$app->get('/Distribuidores[/]', 'getAllDistribuidores');
$app->get('/Assistencias[/]', 'getAllAssistencias');

$app->post('[/]', 'cadastrar');

$app->put('/{id:[0-9]+}', 'put');

function cadastrar(Request $request, Response $response)
{
    $params = $request->getParsedBody();

    $sql = "INSERT INTO assistencias_distribuidores 
                    SET id_cidade = :id_cidade, 
                        id_estado = :id_estado, 
                        nome = :nome, 
                        cep = :cep, 
                        logradouro = :logradouro, 
                        numero = :numero, 
                        bairro = :bairro, 
                        complemento = :complemento,
                        telefone = :telefone, 
                        celular = :celular,
                        email = :email, 
                        is_assistencia = :is_assistencia, 
                        is_distribuidor = :is_distribuidor";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue('id_cidade', $params['id_cidade']);
    $st->bindValue('id_estado', $params['id_estado']);
    $st->bindValue('nome', $params['nome']);
    $st->bindValue('cep', $params['cep']);
    $st->bindValue('logradouro', $params['logradouro']);
    $st->bindValue('numero', $params['numero']);
    $st->bindValue('bairro', $params['bairro']);
    $st->bindValue('complemento', $params['complemento']);
    $st->bindValue('telefone', $params['telefone']);
    $st->bindValue('celular', $params['celular']);
    $st->bindValue('email', $params['email']);
    $st->bindValue('is_distribuidor', $params['is_distribuidor'], PDO::PARAM_BOOL);
    $st->bindValue('is_assistencia', $params['is_assistencia'], PDO::PARAM_BOOL);

    $st->execute();

    return $response->withJson(array(
        "status" => 200,
        "response" => "ok"
    ));

}

function put(Request $request, Response $response, $args)
{
    $params = $request->getParsedBody();

    $sql = "UPDATE assistencias_distribuidores 
                SET id_cidade = :id_cidade, 
                    id_estado = :id_estado, 
                    nome = :nome, 
                    cep = :cep, 
                    logradouro = :logradouro, 
                    numero = :numero, 
                    bairro = :bairro, 
                    telefone = :telefone, 
                    email = :email, 
                    is_assistencia = :is_assistencia, 
                    is_distribuidor = is_distribuidor
              WHERE id = :id";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("id", $args['id']);
    $st->bindValue('id_cidade', $params['id_cidade']);
    $st->bindValue('id_estado', $params['id_estado']);
    $st->bindValue('nome', $params['nome']);
    $st->bindValue('cep', $params['cep']);
    $st->bindValue('logradouro', $params['logradouro']);
    $st->bindValue('numero', $params['numero']);
    $st->bindValue('bairro', $params['bairro']);
    $st->bindValue('complemento', $params['complemento']);
    $st->bindValue('telefone', $params['telefone']);
    $st->bindValue('celular', $params['celular']);
    $st->bindValue('email', $params['email']);
    $st->bindValue('is_distribuidor', $params['is_distribuidor'], PDO::PARAM_BOOL);
    $st->bindValue('is_assistencia', $params['is_assistencia'], PDO::PARAM_BOOL);
    $st->execute();

    return $response->withJson(array(
        "status" => 200,
        "response" => "ok"
    ));
}

function getAll(Request $request, Response $response)
{
    $params = $request->getQueryParams();

    $limit = $params['limit'] ?: 10;
    $page = $params['page'] ?: 1;
    $offset = ($page - 1) * $limit;

    $sqlTotalRows = "SELECT count(id) AS 'total_rows'
                       FROM assistencias_distribuidores
                      WHERE assistencias_distribuidores.ativo = TRUE";

    $stTotal = Conexao::getConnection()->prepare($sqlTotalRows);
    $stTotal->execute();

    $totalRows = $stTotal->fetch(PDO::FETCH_ASSOC);
    $totalRows = $totalRows['total_rows'] ?: 0;

    $numberOfPages = (int)ceil($totalRows / $limit);

    $sql = "SELECT id,
                   id_cidade,
                   id_estado,
                   nome,
                   cep,
                   logradouro,
                   numero,
                   bairro,
                   complemento,
                   telefone,
                   celular,
                   email                 
              FROM assistencias_distribuidores
             WHERE ativo = TRUE
          ORDER BY nome ASC
             LIMIT $offset, $limit";

    $st = Conexao::getConnection()->prepare($sql);
    $st->execute();
    $resultado = $st->fetchAll(PDO::FETCH_ASSOC);

    foreach ($resultado as $key => $value) {
        $resultado[$key]['cidade'] = GeoController::getCidadeById($value['id_cidade'])->name;
        $resultado[$key]['estado'] = GeoController::getEstadoById($value['id_estado'])->name;
    }

    $retorno = array(
        'data' => $resultado,
        'numberOfPages' => $numberOfPages,
        'totalRows' => $totalRows
    );

    return $response->withJson($retorno);
}

function getAllDistribuidores(Request $request, Response $response)
{
    $params = $request->getQueryParams();

    $limit = $params['limit'] ?: 10;
    $page = $params['page'] ?: 1;
    $offset = ($page - 1) * $limit;

    $sqlTotalRows = "SELECT count(id) AS 'total_rows'
                       FROM assistencias_distribuidores
                      WHERE assistencias_distribuidores.ativo = TRUE
                        AND assistencias_distribuidores.is_distribuidor = TRUE";

    $stTotal = Conexao::getConnection()->prepare($sqlTotalRows);
    $stTotal->execute();

    $totalRows = $stTotal->fetch(PDO::FETCH_ASSOC);
    $totalRows = $totalRows['total_rows'] ?: 0;

    $numberOfPages = (int)ceil($totalRows / $limit);

    $sql = "SELECT id,
                   id_cidade,
                   id_estado,
                   nome,
                   cep,
                   logradouro,
                   numero,
                   bairro,
                   complemento,
                   telefone,
                   celular,
                   email
              FROM assistencias_distribuidores
             WHERE ativo = TRUE
               AND is_distribuidor = TRUE
          ORDER BY nome ASC
             LIMIT $offset, $limit";

    $st = Conexao::getConnection()->prepare($sql);
    $st->execute();
    $resultado = $st->fetchAll(PDO::FETCH_ASSOC);

    foreach ($resultado as $key => $value) {
        $resultado[$key]['cidade'] = GeoController::getCidadeById($value['id_cidade'])->name;
        $resultado[$key]['estado'] = GeoController::getEstadoById($value['id_estado'])->name;
    }

    $retorno = array(
        'data' => $resultado,
        'numberOfPages' => $numberOfPages,
        'totalRows' => $totalRows
    );

    return $response->withJson($retorno);
}

function getAllAssistencias(Request $request, Response $response)
{
    $params = $request->getQueryParams();

    $limit = $params['limit'] ?: 10;
    $page = $params['page'] ?: 1;
    $offset = ($page - 1) * $limit;

    $sqlTotalRows = "SELECT count(id) AS 'total_rows'
                       FROM assistencias_distribuidores
                      WHERE assistencias_distribuidores.ativo = TRUE
                        AND assistencias_distribuidores.is_assistencia = TRUE";

    $stTotal = Conexao::getConnection()->prepare($sqlTotalRows);
    $stTotal->execute();

    $totalRows = $stTotal->fetch(PDO::FETCH_ASSOC);
    $totalRows = $totalRows['total_rows'] ?: 0;

    $numberOfPages = (int)ceil($totalRows / $limit);

    $sql = "SELECT id,
                   id_cidade,
                   id_estado,
                   nome,
                   cep,
                   logradouro,
                   numero,
                   bairro,
                   complemento,
                   telefone,
                   celular,
                   email
              FROM assistencias_distribuidores
             WHERE ativo = TRUE
               AND is_assistencia = TRUE
          ORDER BY nome ASC
             LIMIT $offset, $limit";

    $st = Conexao::getConnection()->prepare($sql);
    $st->execute();
    $resultado = $st->fetchAll(PDO::FETCH_ASSOC);

    foreach ($resultado as $key => $value) {
        $resultado[$key]['cidade'] = GeoController::getCidadeById($value['id_cidade'])->name;
        $resultado[$key]['estado'] = GeoController::getEstadoById($value['id_estado'])->name;
    }

    $retorno = array(
        'data' => $resultado,
        'numberOfPages' => $numberOfPages,
        'totalRows' => $totalRows
    );

    return $response->withJson($retorno);
}

$app->run();
