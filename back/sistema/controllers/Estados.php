<?php

include "../../utils/evoxx_autoload.php";
include "../../utils/GeoController.php";
require "../../Seguranca/RestSecure.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App($settingsApp);

$app->get("/", 'getAll');

function getAll(Request $request, Response $response) {
    
    $estados = GeoController::getEstados();
    return $response->withJson($estados);
}

$app->run();
