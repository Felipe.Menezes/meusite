<?php

header("Content-type: application/json;charset=utf-8;");

require "../../utils/evoxx_autoload.php";
require "../../Seguranca/RestSecure.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

ini_set("display_error", 1);

$app = new Slim\App($settingsApp);

$app->get('/', 'getAll');
$app->get('/{id}', 'getNoticia');
$app->post('/', 'adicionarNoticia');
$app->post('/{idNoticia:[0-9]+}/AdicionarFoto[/]', 'adicionarFoto');
$app->put('/{id}', 'putNoticia');
$app->delete('/{id}', 'deleteNoticia');
$app->delete('/{idFoto:[0-9]+}/RemoverFoto[/]', 'removerFoto');

define("DIRECTORY_IMAGES_NOTICIAS", DIRECTORY_IMAGES . "noticias/");
define("URL_IMAGES", PUBLIC_URL . "images/noticias/");

function adicionarFoto(Request $request, Response $response, $args)
{

    $files = $request->getUploadedFiles();

    $sql = "INSERT INTO noticias_foto
                    SET id_noticia = :id_noticia,
                        foto = :foto";

    foreach ($files['imagens'] as $uploadedFile) {
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = moveUploadedFile(DIRECTORY_IMAGES_NOTICIAS, $uploadedFile);

            $st = Conexao::getConnection()->prepare($sql);
            $st->bindValue("id_noticia", $args['idNoticia']);
            $st->bindValue("foto", $filename);
            $st->execute();
        }
    }

    $resposta = array(
        'status' => 200,
        'response' => "Cadastrado com sucesso!"
    );

    return $response->withJson($resposta);
}

function removerFoto(Request $request, Response $response, $args)
{
    $nomeFoto = "";

    $sqlSelect = "SELECT foto 
                    FROM noticias_foto 
                   WHERE id = :id";

    $stSelect = Conexao::getConnection()->prepare($sqlSelect);
    $stSelect->bindValue("id", $args['idFoto']);
    $stSelect->execute();

    $fetch = $stSelect->fetch(PDO::FETCH_ASSOC);
    $nomeFoto = $fetch['foto'];

    echo unlink(DIRECTORY_IMAGES_NOTICIAS . $nomeFoto);

    $sql = "DELETE FROM noticias_foto
                  WHERE id = :id";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("id", $args['idFoto']);
    $st->execute();

    $response->getBody()->write(json_encode(array(
        'response' => "Apagado com sucesso!"
    )));

    return $response;
}

function putNoticia(Request $request, Response $response, $args)
{
    if (!isset($args['id']) || empty($args['id']) || !is_numeric($args['id'])) return erro("Id inválido!");

    $params = json_decode($request->getBody());

    if (!isset($params->id_autor) || empty($params->id_autor)) return erro($response, "Autor obrigatório");
    if (!isset($params->titulo) || empty($params->titulo)) return erro($response, "Título obrigatório");
    if (!isset($params->artigo) || empty($params->artigo)) return erro($response, "Artigo obrigatório");
    if (!isset($params->data_publicacao) || empty($params->data_publicacao)) return erro($response, "Data de publicação obrigatória");

    $dataPublicacao = date_create_from_format("d/m/Y H:i:s", $params->data_publicacao);

    if (!$dataPublicacao) return erro($response, "Formato de data inválida.");

    $sql = "UPDATE noticias
               SET id_autor = :id_autor,
                   titulo = :titulo,
                   artigo = :artigo,
                   publicacao = :data_publicacao
             WHERE id = :id
               AND ativo = TRUE";

    try {
        $st = Conexao::getConnection()->prepare($sql);
        $st->bindValue('id_autor', $params->id_autor);
        $st->bindValue('titulo', $params->titulo);
        $st->bindValue('artigo', $params->artigo);
        $st->bindValue('data_publicacao', $dataPublicacao->format('Y-m-d H:i:s'), PDO::PARAM_STR);
        $st->bindValue('id', $args['id']);
        $alterou = $st->execute();
    } catch (PDOException $e) {
        ResponseHTTP::error("Não foi possível alterar o registro!");
    }

    $resposta = array(
        'status' => 200,
        'response' => "Alterado com sucesso!"
    );

    $response->getBody()->write(json_encode($resposta));
    return $response;
}

function deleteNoticia(Request $request, Response $response, $args)
{
    if (!isset($args['id']) || empty($args['id']) || !is_numeric($args['id'])) return erro("Id inválido!");

    $sql = "UPDATE noticias
               SET ativo = FALSE
             WHERE id = :id
               AND ativo = TRUE";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("id", $args['id']);
    $result = $st->execute();

    $response->getBody()->write(json_encode(array(
        'response' => "Apagado com sucesso!"
    )));

    return $response;
}

function getNoticia(Request $request, Response $response, $args)
{
    if (!isset($args['id']) || empty($args['id']) || !is_numeric($args['id'])) return erro("Id inválido!");

    $sql = "SELECT noticias.id,
                  noticias.id_autor,
                  autor.nome_completo AS autor,
                  titulo,
                  artigo,
                  publicacao AS data_publicacao
             FROM noticias
        LEFT JOIN usuarios autor
               ON noticias.id_autor = autor.id
            WHERE noticias.ativo = TRUE
              AND noticias.id = :id";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue('id', $args['id']);
    $st->execute();

    $resultado = $st->fetchAll(PDO::FETCH_ASSOC);

    if (count($resultado) > 0) {
        $dataPublicacao = date_create_from_format('Y-m-d H:i:s', $resultado[0]['data_publicacao']);
        $resultado[0]['data_publicacao'] = $dataPublicacao->format("d/m/Y H:i:s");
        $resultado[0]['fotos'] = array();

        $sqlFotos = "SELECT id,
                            foto
                       FROM noticias_foto
                      WHERE id_noticia = :id
                        AND ativo = TRUE
                   ORDER BY id";

        $stFotos = Conexao::getConnection()->prepare($sqlFotos);
        $stFotos->bindValue('id', $resultado[0]['id']);
        $stFotos->execute();
        $fotos = $stFotos->fetchAll(PDO::FETCH_ASSOC);

        foreach ($fotos as $ft) {
            $ft['foto'] = URL_IMAGES . $ft['foto'];
            array_push($resultado[0]['fotos'], $ft);
        }

        $retorno = json_encode($resultado[0]);


        $response->getBody()->write($retorno);
    } else {
        http_response_code(401);
        $retorno = json_encode(array(
            "status" => 401,
            "Notícia inexistente!"
        ));
        die($retorno);
    }

    return $response;
}

function getAll(Request $request, Response $response)
{
    $params = (object)$request->getQueryParams();

    $limit = $params->limit ?: 10;
    $page = $params->page ?: 1;
    $offset = ($page - 1) * $limit;

    $sqlTotalRows = "SELECT count(id) AS 'total_rows'
                       FROM noticias
                      WHERE noticias.ativo = TRUE";

    $stTotal = Conexao::getConnection()->prepare($sqlTotalRows);
    $stTotal->execute();

    $totalRows = $stTotal->fetch(PDO::FETCH_ASSOC);
    $totalRows = $totalRows['total_rows'] ?: 0;

    $numberOfPages = (int)ceil($totalRows / $limit);

    $sql = "SELECT noticias.id,
                   noticias.id_autor,
                   autor.nome_completo AS autor,
                   titulo,
                   artigo,
                   publicacao AS data_publicacao
              FROM noticias
         LEFT JOIN usuarios autor
                ON noticias.id_autor = autor.id
             WHERE noticias.ativo = true
             LIMIT $offset, $limit";


    $stResultado = Conexao::getConnection()->prepare($sql);
    $stResultado->execute();
    $resultado = $stResultado->fetchAll(PDO::FETCH_ASSOC);

    $data = array();

    foreach ($resultado as $item) {
        $dataPublicacao = date_create_from_format('Y-m-d H:i:s', $item['data_publicacao']);
        $item['data_publicacao'] = $dataPublicacao->format("d/m/Y H:i:s");

        array_push($data, $item);
    }

    $retorno = array(
        'data' => $data,
        'numberOfPages' => $numberOfPages,
        'totalRows' => $totalRows
    );

    $response->getBody()->write(json_encode($retorno));

    return $response;

}

function adicionarNoticia(Request $request, Response $response)
{
    $params = (object)$request->getParsedBody();

    if (!isset($params->id_autor) || empty($params->id_autor)) return erro($response, "Autor obrigatório");
    if (!isset($params->titulo) || empty($params->titulo)) return erro($response, "Título obrigatório");
    if (!isset($params->artigo) || empty($params->artigo)) return erro($response, "Artigo obrigatório");
    if (!isset($params->data_publicacao) || empty($params->data_publicacao)) return erro($response, "Data de publicação obrigatória");

    $dataPublicacao = date_create_from_format("d/m/Y H:i:s", $params->data_publicacao);

    if (!$dataPublicacao) return erro($response, "Formato de data inválida.");

    $sql = "INSERT INTO noticias
                    SET id_autor = :id_autor,
                        titulo = :titulo,
                        artigo = :artigo,
                        publicacao = :publicacao";

    $db = Conexao::getConnection();

    $st = $db->prepare($sql);
    $st->bindValue("id_autor", $params->id_autor);
    $st->bindValue("titulo", $params->titulo);
    $st->bindValue("artigo", $params->artigo);
    $st->bindValue("publicacao", $dataPublicacao->format('Y-m-d H:i:s'), PDO::PARAM_STR);

    $st->execute();

    $idNoticiaNova = $db->lastInsertId();

    $resposta = array(
        "status" => 200,
        "response" => "ok"
    );

    $files = $request->getUploadedFiles();

    $sql = "INSERT INTO noticias_foto
                    SET id_noticia = :id_noticia,
                        foto = :foto";

    foreach ($files['imagens'] as $uploadedFile) {
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = moveUploadedFile(DIRECTORY_IMAGES_NOTICIAS, $uploadedFile);

            $st = Conexao::getConnection()->prepare($sql);
            $st->bindValue("id_noticia", $idNoticiaNova);
            $st->bindValue("foto", $filename);
            $st->execute();
        }
    }

    $response->getBody()->write(json_encode($resposta));

    return $response;

}

function erro($response, $mensagem)
{
    $response->getBody()->write(json_encode(array(
        "status" => 401,
        "response" => $mensagem
    )));

    return false;
}

$app->run();

?>