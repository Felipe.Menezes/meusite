<?php

include "../../utils/evoxx_autoload.php";
include "../../utils/GeoController.php";
require "../../Seguranca/RestSecure.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App($settingsApp);

$app->get("/porEstado/{idEstado}", 'getByEstado');

function getByEstado(Request $request, Response $response, $args) {
    $cidades = GeoController::getCidadesByEstado($args['idEstado']);
    return $response->withJson($cidades);
}

$app->run();
