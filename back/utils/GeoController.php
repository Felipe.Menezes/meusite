<?php

class GeoController {
    private static function compare($a, $b){
        return strcmp($a->name, $b->name);
    }
    
    private static function getCidades(){
        $file = file_get_contents("../../utils/cities.json");
        $decoded = json_decode($file);
        usort($decoded, array(self::class, 'compare'));
        return $decoded;
    }

    static function getEstados(){
        $file = file_get_contents("../../utils/states.json");
        $decoded = json_decode($file);
        usort($decoded, array(self::class, 'compare'));
        return $decoded;
    }   

    static function getEstadoById($idEstado){
        foreach(self::getEstados() as $estado){
            if($estado->id == $idEstado)
                return $estado;
        }
        return new StdClass();
    }

    static function getCidadesByEstado($idEstado){
        $retorno = array();

        foreach(self::getCidades() as $cidade){
            if($cidade->state_id == $idEstado)
                array_push($retorno, $cidade);
        }
        
        return $retorno;
    }

    static function getCidadeById($idCidade){
        foreach(self::getCidades() as $cidade){
            if($cidade->id == $idCidade)
                return $cidade;
        }
        return new StdClass();
    }
}