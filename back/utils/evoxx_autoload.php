<?php

use Slim\Http\UploadedFile;

header("Content-type: application/json;charset=utf-8;");

require "../../vendor/autoload.php";
require "../../Seguranca/JWT.php";
require "../../Conexao.class.php";
require "../../utils/Response.class.php";

define("PUBLIC_URL", "http://demo.evoxx.com.br/");
define("DIRECTORY_IMAGES", "/home/evoxx247/demo.evoxx.com.br/images/");
define("DIRECTORY_FILES", "/home/evoxx247/demo.evoxx.com.br/files/");

$settingsApp = [
    'settings' => [
        // Slim Settings
        'determineRouteBeforeAppMiddleware' => true,
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,
    ]
];

function moveUploadedFile($directory, UploadedFile $uploadedFile)
{
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
    $filename = sprintf('%s.%0.8s', $basename, $extension);

    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    chmod($directory . DIRECTORY_SEPARATOR . $filename, 0777);

    return $filename;
}