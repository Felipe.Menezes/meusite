<?php

class ResponseHTTP {

    public static function error($message, $inner = "", $code = 401){
        http_response_code($code);
        die(json_encode(array(
            "message" => $message,
            "inner" => $inner
        )));
    }
}