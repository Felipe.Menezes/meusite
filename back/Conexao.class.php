<?php

error_reporting(E_ERROR);

class Conexao
{
    private static $HOST = "localhost";
    //private static $USER = "root";
    private static $USER = "evoxx247_root";
    //private static $PASS = "Junior1996@sql";
    private static $PASS = "n4w9c6Ju3Z";
    private static $DB = "evoxx247_evoxx";

    public static function getConnection()
    {
        try {
            $con = new PDO("mysql:dbname=" . self::$DB . ";host=" . self::$HOST . ";charset=utf8;", self::$USER, self::$PASS);
            $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $con;
        } catch (PDOException $e) {
            die("Connection failed: " . $e->getMessage());
        }

    }
}