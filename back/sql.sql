SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `configuracoes_site` (
  `id` int(11) NOT NULL,
  `conheca_nossos_produtos` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `newsletter_email` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `noticias` (
  `id` int(11) NOT NULL,
  `id_autor` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `artigo` text COLLATE utf8_unicode_ci NOT NULL,
  `publicacao` datetime NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `noticias_foto` (
  `id` int(11) NOT NULL,
  `id_noticia` int(11) NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL,
  `nome_completo` text COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `senha` text COLLATE utf8_unicode_ci NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `configuracoes_site`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `newsletter_email`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_noticias_autor` (`id_autor`);

ALTER TABLE `noticias_foto`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_foto_noticias` (`id_noticia`);

ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `usuarios_login_uindex` (`login`);

ALTER TABLE `configuracoes_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

ALTER TABLE `newsletter_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;

ALTER TABLE `noticias_foto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;


/* ---------------------------------------- */

CREATE TABLE IF NOT EXISTS produtos_categorias(
  id INT PRIMARY KEY AUTO_INCREMENT,
  descricao VARCHAR(255) NOT NULL,
  ativo BOOLEAN NOT NULL DEFAULT true
);

CREATE TABLE IF NOT EXISTS produtos(
  id INT PRIMARY KEY AUTO_INCREMENT,
  id_categoria INT NOT NULL,
  descricao VARCHAR (255) NOT NULL,
  subdescricao TEXT,
  manual VARCHAR (255),
  ativo BOOLEAN NOT NULL DEFAULT true,
  CONSTRAINT fk_produtos_categoria
  FOREIGN KEY (id_categoria) REFERENCES produtos_categorias(id)
);

CREATE TABLE IF NOT EXISTS produtos_fotos(
  id INT PRIMARY KEY AUTO_INCREMENT,
  id_produto INT NOT NULL,
  foto VARCHAR(255) NOT NULL,
  ativo BOOLEAN NOT NULL DEFAULT true,
  CONSTRAINT fk_foto_produtos
  FOREIGN KEY (id_produto) REFERENCES produtos(id)
);

CREATE TABLE IF NOT EXISTS produtos_tipo_caracteristicas(
  id INT PRIMARY KEY AUTO_INCREMENT,
  descricao VARCHAR(255) NOT NULL,
  ativo BOOLEAN NOT NULL DEFAULT true
);

CREATE TABLE IF NOT EXISTS produtos_caracteristicas(
  id INT PRIMARY KEY AUTO_INCREMENT,
  id_tipo INT NOT NULL,
  id_produto INT NOT NULL,
  caracteristica TEXT NOT NULL,
  ativo BOOLEAN NOT NULL DEFAULT true,
  CONSTRAINT fk_caracteristicas_tipo
  FOREIGN KEY (id_tipo) REFERENCES produtos_tipo_caracteristicas(id),
  CONSTRAINT fk_caracteristicas_produto
  FOREIGN KEY (id_produto) REFERENCES produtos(id)
);

CREATE TABLE IF NOT EXISTS produtos_acessorios(
  id INT PRIMARY KEY AUTO_INCREMENT,
  id_produto INT NOT NULL,
  acessorio VARCHAR(255),
  ativo BOOLEAN NOT NULL DEFAULT true,
  CONSTRAINT fk_acessorios_produto
  FOREIGN KEY (id_produto) REFERENCES produtos(id)
);

CREATE TABLE IF NOT EXISTS produtos_especificacoes(
  id INT PRIMARY KEY AUTO_INCREMENT,
  id_produto INT NOT NULL,
  titulo VARCHAR(255),
  valor VARCHAR(255),
  ativo BOOLEAN NOT NULL DEFAULT true,
  CONSTRAINT fk_especificacoes_produto
  FOREIGN KEY (id_produto) REFERENCES produtos(id)
);

CREATE TABLE IF NOT EXISTS institucional_quem_somos(
  id INT PRIMARY KEY AUTO_INCREMENT,
  titulo TEXT NOT NULL,
  artigo TEXT NOT NULL,
  ativo BOOLEAN NOT NULL DEFAULT true
);

CREATE TABLE IF NOT EXISTS assistencias_distribuidores(
  id INT PRIMARY KEY AUTO_INCREMENT,
  id_cidade INT NOT NULL,
  id_estado INT NOT NULL,
  nome VARCHAR(255) NOT NULL,
  cep INT NOT NULL,
  logradouro VARCHAR(255) NOT NULL,
  numero VARCHAR(50) NOT NULL,
  bairro VARCHAR(100) NOT NULL,
  complemento VARCHAR(255),
  telefone VARCHAR(30) NOT NULL,
  celular VARCHAR(30),
  email VARCHAR(200) NOT NULL,
  is_assistencia BOOLEAN DEFAULT FALSE,
  is_distribuidor BOOLEAN DEFAULT FALSE,
  ativo BOOLEAN NOT NULL DEFAULT true
);

INSERT INTO configuracoes_site SET id = 1, conheca_nossos_produtos ='Tradição e Qualidade na Indústria de Equipamentos Odontológicos.';
INSERT INTO usuarios SET id = 1, nome_completo = 'Administrador do site', email = 'admin@evoxx.com.br', login = 'admin', senha = '21232f297a57a5a743894a0e4a801fc3', ativo = 1;
INSERT INTO institucional_quem_somos SET titulo = 'A Evoxx, desde a sua fundação, juntamente com seus colaboradores, preocupa-se em fabricar equipamentos periféricos odontológicos visando a plena satisfação de seus clientes.', artigo = 'Nossa <b>missão</b> é proporcionar conforto e praticidade ao profissional, desenvolvendo novas tecnologias em produtos odontológicos baseado na ética e credibilidade, atendendo suas necessidades e objetivando uma melhor qualidade de vida a todos. Nossa empresa vem se destacando como uma das melhores alternativas em seu segmento. A linha de produtos foi, ao longo do tempo, se definindo em resposta às necessidades de nossos clientes. Os produtos Schuster são conhecidos pela sua qualidade e eficiência, sendo que nossa preocupação constante em buscar novas tecnologias tem sido responsável pela evolução contínua dos produtos fabricados.';
INSERT INTO produtos_categorias SET descricao = 'Categoria 1';
INSERT INTO produtos_categorias SET descricao = 'Categoria 2';
INSERT INTO produtos_categorias SET descricao = 'Categoria 3';
INSERT INTO produtos SET id_categoria = 1 , descricao = 'Produto 1', subdescricao = 'SubDescrição 1';
INSERT INTO produtos SET id_categoria = 1 , descricao = 'Produto 2', subdescricao = 'SubDescrição 2';
INSERT INTO produtos SET id_categoria = 1 , descricao = 'Produto 3', subdescricao = 'SubDescrição 3';
INSERT INTO produtos_acessorios SET id_produto = 1, acessorio = '1 Cabo de medição';
INSERT INTO produtos_especificacoes SET id_produto = 1, titulo = 'Frequência', valor = '50/60 Hz';
INSERT INTO assistencias_distribuidores SET id_cidade = 4115200, id_estado = 41, nome = 'Assistência técnica', cep = 87010000, logradouro = 'Av Cerro Azul', numero = '123A', bairro = 'Centro', telefone = '(44) 3244-4444', email = 'contato@evoxx.com.br', is_assistencia = TRUE, is_distribuidor = TRUE;