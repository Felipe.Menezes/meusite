<?php

header("Content-type: application/json;charset=utf-8;");

include "../../utils/evoxx_autoload.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App();

$app->post("/registrar", 'Registrar');

function Registrar(Request $request, Response $response){
    $requestParams = json_decode($request->getBody());
    $email = $requestParams->email;

    if(empty($email)){
        return json_encode(array(
            'response' => "Email was empty!"
            )
        );
    }

    $sqlCheck = "SELECT id
                   FROM newsletter_email
                  WHERE email = :email
                    AND ativo = true";

    $stCheck = Conexao::getConnection()->prepare($sqlCheck);
    $stCheck->bindValue("email", $email);
    $stCheck->execute();
    $existe = $stCheck->fetchAll(PDO::FETCH_ASSOC);

    if($existe){
        $resposta = array(
            "response" => "Email já cadastrado!"
        );
        http_response_code(500);
        die(json_encode($resposta));
    }

    $sql = "INSERT INTO newsletter_email
                    SET email = :email";

    $statement = Conexao::getConnection()->prepare($sql);
    $statement->bindValue("email", $email);
    $statement->execute();
    
    return json_encode(array(
        "response" => "Cadastrado com sucesso!"
    ));
    
}

$app->run();