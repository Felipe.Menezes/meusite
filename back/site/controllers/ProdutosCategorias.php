<?php

include "../../utils/evoxx_autoload.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

header("Content-type: application/json;charset=utf-8;");

$app = new \Slim\App();

$app->get("/", 'get');
$app->get("/{id}", 'getById');

function get(Request $request, Response $response) {
	$sql = "SELECT id,
				   descricao
			  FROM produtos_categorias
			 WHERE ativo = true
		  ORDER BY descricao";

	try {
		$st = Conexao::getConnection()->prepare($sql);
		$st->execute();

		$resultado = $st->fetchAll(PDO::FETCH_ASSOC);

		$response->getBody()->write(json_encode($resultado));
		
		return $response;

	} catch(PDOException $e) {
		ResponseHTTP::error("Ocorreu um erro ao obter as categorias!", $e->getMessage());
	}
}

function getById(Request $request, Response $response, $args){

	$sql = "SELECT id,
				   descricao
			  FROM produtos_categorias
			 WHERE id = :id
			   AND ativo = true
			 LIMIT 1";

	try {
		$st = Conexao::getConnection()->prepare($sql);
		$st->bindValue("id", $args['id']);
		$st->execute();

		$resultado = $st->fetchAll(PDO::FETCH_ASSOC);

		$response->getBody()->write(json_encode($resultado));

		return $response;

	} catch(PDOException $e) {
		ResponseHTTP::error("Ocorreu um erro ao obter a categoria!", $e->getMessage());
	}
}

$app->run();