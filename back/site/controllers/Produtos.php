<?php

include "../../utils/evoxx_autoload.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

header("Content-type: application/json;charset=utf-8;");

define("URL_IMAGES", PUBLIC_URL . "images/produtos/");
define("URL_FILES", PUBLIC_URL . "files/produtos/");

$app = new \Slim\App([
    'settings' => [
        // Slim Settings
        'determineRouteBeforeAppMiddleware' => true,
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,
    ]
]);

$app->get("[/]", 'getAll');
$app->get("/{id}", 'getById');
$app->get("/buscarPorCategoria/{id}", 'getByCategoria');
$app->get("/{id:[0-9]+}/Caracteristicas", 'getCaracteristicas');
$app->get("/{id:[0-9]+}/Especificacoes", 'getEspecificacoes');

function getCaracteristicas(Request $request, Response $response, $args)
{
    $sql = "SELECT produtos_tipo_caracteristicas.id,
                   produtos_tipo_caracteristicas.descricao 
              FROM produtos_caracteristicas
         LEFT JOIN produtos_tipo_caracteristicas
                ON produtos_tipo_caracteristicas.id = produtos_caracteristicas.id_tipo
             WHERE id_produto = :id_produto
               AND produtos_tipo_caracteristicas.ativo = TRUE
               AND produtos_caracteristicas.ativo = TRUE
          GROUP BY produtos_tipo_caracteristicas.id";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("id_produto", $args['id']);
    $st->execute();
    $resultado = $st->fetchAll(PDO::FETCH_ASSOC);

    foreach ($resultado as $key => $item) {
        $sqlItem = "SELECT caracteristica
                      FROM produtos_caracteristicas
                     WHERE id_tipo = :id_tipo
                       AND id_produto = :id_produto
                       AND ativo = TRUE";

        $stItem = Conexao::getConnection()->prepare($sqlItem);
        $stItem->bindValue("id_tipo", $item['id']);
        $stItem->bindValue("id_produto", $args['id']);
        $stItem->execute();

        $resultado[$key]['items'] = $stItem->fetchAll(PDO::FETCH_ASSOC);
    }

    return $response->withJson($resultado);
}

function getEspecificacoes(Request $request, Response $response, $args)
{
    $sql = "SELECT titulo,
                   valor
              FROM produtos_especificacoes
             WHERE id_produto = :id_produto
               AND ativo = TRUE";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("id_produto", $args['id']);
    $st->execute();
    $resultado = $st->fetchAll(PDO::FETCH_ASSOC);

    return $response->withJson($resultado);
}

function getById(Request $request, Response $response, $args)
{
    $sql = "SELECT id,
				   id_categoria,
				   descricao,
				   manual,
				   subdescricao
			  FROM produtos
			 WHERE id = :id
			   AND ativo = true
			 LIMIT 1";

    try {
        $st = Conexao::getConnection()->prepare($sql);
        $st->bindValue("id", $args['id']);
        $st->execute();

        $resultado = $st->fetch(PDO::FETCH_ASSOC);

        $sqlFotos = "SELECT foto 
                       FROM produtos_fotos 
                      WHERE id_produto = :id_produto 
                        AND ativo = TRUE";

        $stFotos = Conexao::getConnection()->prepare($sqlFotos);
        $stFotos->bindValue("id_produto", $resultado['id']);
        $stFotos->execute();
        $resultadoFoto = $stFotos->fetchAll(PDO::FETCH_ASSOC);
        $resultado["fotos"] = array();

        foreach ($resultadoFoto as $foto) {
            $linkFoto = URL_IMAGES . $foto['foto'];
            array_push($resultado["fotos"], $linkFoto);
        }

        $resultado['linkManual'] = URL_FILES . $resultado['manual'];

        $response->getBody()->write(json_encode($resultado));

        return $response;
    } catch (PDOException $e) {
        ResponseHTTP::error("Erro ao obter o categoria!", $e->getMessage());
    }
}

function getByCategoria(Request $request, Response $response, $args)
{
    $sql = "SELECT produtos.id, 
				   descricao, 
				   id_categoria, 
				   subdescricao ,
				   IFNULL((SELECT foto 
				   	  FROM produtos_fotos 
				   	 WHERE id_produto = produtos.id 
				  ORDER BY produtos_fotos.id DESC 
				  	 LIMIT 1), '') AS foto
			  FROM produtos 
			 WHERE id_categoria = :id_categoria
			   AND produtos.ativo = true ";

    $st = Conexao::getConnection()->prepare($sql);
    $st->bindValue("id_categoria", $args['id']);
    $st->execute();

    $resultado = $st->fetchAll(PDO::FETCH_ASSOC);

    foreach ($resultado as $key => $item) {
        if (!empty($item['foto']))
            $resultado[$key]['foto'] = URL_IMAGES . $item['foto'];
    }

    $response->getBody()->write(json_encode($resultado));

    return $response;
}

function getAll(Request $request, Response $response)
{
    $sql = "SELECT produtos.id, 
				   descricao, 
				   id_categoria, 
				   subdescricao ,
				   IFNULL((SELECT foto 
				   	  FROM produtos_fotos 
				   	 WHERE id_produto = produtos.id 
				  ORDER BY produtos_fotos.id DESC 
				  	 LIMIT 1), '') AS foto
			  FROM produtos 
			 WHERE produtos.ativo = true ";

    try {
        $st = Conexao::getConnection()->prepare($sql);
        $st->execute();

        $resultado = $st->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resultado as $key => $item) {
            if (!empty($item['foto']))
                $resultado[$key]['foto'] = URL_IMAGES . $item['foto'];
        }

        return $response->withJson($resultado);
    } catch (PDOException $e) {
        ResponseHTTP::error("Erro ao obter os produtos", $e->getMessage());
    }

    return $response;
}

$app->run();