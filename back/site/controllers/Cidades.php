<?php

include "../../utils/evoxx_autoload.php";
include "../GeoController.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App();

$app->get("/porEstado/{idEstado}", 'getByEstado');

function getByEstado(Request $request, Response $response, $args) {
    $cidades = GeoController::getCidadesByEstado($args['idEstado']);
    $response->getBody()->write(json_encode($cidades));
    return $response;
}

$app->run();
