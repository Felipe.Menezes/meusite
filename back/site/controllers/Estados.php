<?php

include "../../utils/evoxx_autoload.php";
include "../GeoController.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App();

$app->get("/", 'getAll');

function getAll(Request $request, Response $response) {
    
    $estados = GeoController::getEstados();
    $response->getBody()->write(json_encode($estados));
    return $response;
}

$app->run();
