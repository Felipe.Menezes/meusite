<?php

require "../utils/evoxx_autoload.php";
require "../Seguranca/RestSecure.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

header("Content-type: application/json;charset=utf-8;");

$app = new Slim\App();

$app->get("/", 'RecuperarUsuarios');

function RecuperarUsuarios(Request $request, Response $response)
{

    $sql = "SELECT id,
                   nome_completo,
                   email,
                   login,
                   ativo
              FROM usuarios";

    $st = Conexao::getConnection()->prepare($sql);
    $st->execute();
    $allUsers = $st->fetchAll(PDO::FETCH_ASSOC);

    $response->getBody()->write(json_encode($allUsers));

    return $response;
}

$app->run();