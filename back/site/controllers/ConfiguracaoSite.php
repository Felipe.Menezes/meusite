<?php

include "../../utils/evoxx_autoload.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

header("Content-type: application/json;charset=utf-8;");

$app = new \Slim\App();

$app->get("/", 'get');

function get(Request $request, Response $response) {
  $sql = "SELECT conheca_nossos_produtos
            FROM configuracoes_site
           LIMIT 1";

  try{

    $st = Conexao::getConnection()->prepare($sql);
    $st->execute();
    return json_encode($st->fetch(PDO::FETCH_ASSOC));

  } catch (PDOException $e) {
    http_response_code(500);
    die(json_encode(array(
        "status" => 500,
        "response" => $e->getMessage()
    )));
  }
}

$app->run();
