<?php

include "../../utils/evoxx_autoload.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App($settingsApp);

$app->get('/', 'getListagem');
$app->get('/forIndex', 'getForIndex');
$app->get('/{id}', 'getById');

define("URL_IMAGES", PUBLIC_URL . "images/noticias/");

function cutText($text, $maxLength)
{
    if (strlen($text) > $maxLength) {
        $offset = ($maxLength - 3) - strlen($text);
        $text = substr($text, 0, strrpos($text, ' ', $offset)) . '...';
    }
    return $text;
}

function getById(Request $request, Response $response, $args)
{
    $params = (object)$args;
    $id = $params->id;

    try {
        $sql = "SELECT id,
                       titulo,
                       artigo
                  FROM noticias
                 WHERE id = :id
                   AND ativo = TRUE";

        $stPrincipal = Conexao::getConnection()->prepare($sql);
        $stPrincipal->bindValue("id", $id);
        $stPrincipal->execute();
        $resultado = $stPrincipal->fetch(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        ResponseHTTP::error("Erro ao obter a notícia", $e->getMessage());
    }

    try {
        $sqlFotos = "SELECT foto
                       FROM noticias_foto
                      WHERE id_noticia = :id
                        AND ativo = TRUE
                   ORDER BY rand()";

        $stFotos = Conexao::getConnection()->prepare($sqlFotos);
        $stFotos->bindValue('id', $id);
        $stFotos->execute();
        $resultadoFotos = $stFotos->fetchAll(PDO::FETCH_ASSOC);
        $resultado['fotos'] = array();

        foreach ($resultadoFotos as $item) {
            array_push($resultado['fotos'], URL_IMAGES . $item['foto']);
        }

    } catch (PDOException $e) {
        ResponseHTTP::error("Erro ao obter as fotos", $e->getMessage());
    }

    $response->getBody()->write(json_encode($resultado));
    return $response;
}

function getListagem(Request $request, Response $response)
{
    $params = (object)$request->getQueryParams();

    $limit = $params->limit ?: 10;
    $page = $params->page ?: 1;
    $offset = ($page - 1) * $limit;

    $sqlTotalRows = "SELECT count(id) AS 'total_rows'
                       FROM noticias
                      WHERE noticias.ativo = TRUE";

    $stTotal = Conexao::getConnection()->prepare($sqlTotalRows);
    $stTotal->execute();

    $totalRows = $stTotal->fetch(PDO::FETCH_ASSOC);
    $totalRows = $totalRows['total_rows'] ?: 0;

    $numberOfPages = (int)ceil($totalRows / $limit);

    $sql = "SELECT noticias.id,
                   noticias.id_autor,
                   autor.nome_completo AS autor,
                   titulo,
                   artigo,
                   publicacao AS data_publicacao
              FROM noticias
         LEFT JOIN usuarios autor
                ON noticias.id_autor = autor.id
             WHERE noticias.ativo = true
             LIMIT $offset, $limit";


    $stResultado = Conexao::getConnection()->prepare($sql);
    $stResultado->execute();
    $resultado = $stResultado->fetchAll(PDO::FETCH_ASSOC);

    $sqlFotos = "SELECT foto
                   FROM noticias_foto
                  WHERE id_noticia = :id
               ORDER BY rand()";

    $data = array();

    foreach ($resultado as $item) {
        $dataPublicacao = date_create_from_format('Y-m-d H:i:s', $item['data_publicacao']);
        $item['data_publicacao'] = $dataPublicacao->format("d/m/Y H:i:s");
        $item['artigo'] = cutText($item['artigo'], 100);

        try {
            $stFotos = Conexao::getConnection()->prepare($sqlFotos);
            $stFotos->bindValue("id", $item['id']);
            $stFotos->execute();
            $resultFotos = $stFotos->fetchAll(PDO::FETCH_ASSOC);
            $item['fotos'] = array();

            foreach ($resultFotos as $foto) {

                array_push($item['fotos'], URL_IMAGES . $foto['foto']);
            }

        } catch (PDOException $e) {
            ResponseHTTP::error("Erro nas fotos #$item[id]", $e->getMessage());
        }

        array_push($data, $item);
    }

    $retorno = array(
        'data' => $data,
        'numberOfPages' => $numberOfPages,
        'totalRows' => $totalRows
    );

    $response->getBody()->write(json_encode($retorno));

    return $response;

}

function getForIndex(Request $request, Response $response)
{
    $sql = "SELECT id,
                   titulo,
                   artigo,
                   publicacao AS data_publicacao
              FROM noticias
             WHERE ativo = true
          ORDER BY publicacao DESC, id DESC
             LIMIT 3";

    try {
        $st = Conexao::getConnection()->prepare($sql);
        $st->execute();
        $retorno = $st->fetchAll(PDO::FETCH_ASSOC);

        $sqlFotos = "SELECT foto
                   FROM noticias_foto
                  WHERE id_noticia = :id
               ORDER BY rand()";

        $data = array();

        foreach ($retorno as $item) {
            $dataPublicacao = date_create_from_format('Y-m-d H:i:s', $item['data_publicacao']);
            $item['data_publicacao'] = $dataPublicacao->format("d/m/Y H:i:s");
            $item['artigo'] = cutText($item['artigo'], 100);

            try {
                $stFotos = Conexao::getConnection()->prepare($sqlFotos);
                $stFotos->bindValue("id", $item['id']);
                $stFotos->execute();
                $resultFotos = $stFotos->fetchAll(PDO::FETCH_ASSOC);

                foreach ($resultFotos as $key => $value) {
                    $resultFotos[$key]['foto'] = URL_IMAGES . $value['foto'];
                }

                if ($resultFotos)
                    $item['foto'] = $resultFotos[0]['foto'];
                else
                    $item['foto'] = "";
            } catch (PDOException $e) {
                ResponseHTTP::error("Erro nas fotos #$item[id]", $e->getMessage());
            }

            array_push($data, $item);
        }

        $response->getBody()->write(json_encode($data));
    } catch (PDOException $e) {
        ResponseHTTP::error("Erro ao obter as notícias", $e->getMessage());
    }

    return $response;
}

$app->run();