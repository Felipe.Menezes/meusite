<?php

include "../utils/evoxx_autoload.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

header("Content-type: application/json;charset=utf-8;");

$app = new \Slim\App();

$app->post('/logar', 'logar');

function logar(Request $request, Response $response)
{

    $requestParams = (object)$request->getParsedBody();

    if (!isset($requestParams->login) || !isset($requestParams->senha)) {
        http_response_code(400);
        die(json_encode(array(
                "status" => 400,
                "response" => "Defina os campos login e senha"
            )
        ));
    }

    $login = $requestParams->login;
    $senha = $requestParams->senha;

    $sql = "SELECT id
              FROM usuarios
             WHERE login = :login
               AND senha = md5(:senha)
               AND ativo = true";

    $statement = Conexao::getConnection()->prepare($sql);
    $statement->bindParam("login", $login);
    $statement->bindParam("senha", $senha);
    $statement->execute();

    $fetch = $statement->fetch(PDO::FETCH_ASSOC);

    if (!count($statement)) {
        http_response_code(400);
        die(json_encode(array(
            "status" => 400,
            "response" => "Usuário e/ou senha inválido(s)."
        )));
    }

    $resposta = array(
        "token" => getIdToken($fetch['id'])
    );

    $response->getBody()->write(json_encode($resposta));

    return $response;
}

function getIdToken($idUsuario)
{
    $data = array(
        "data" => date('m/d/Y h:i:s a', time()),
        "idUsuario" => $idUsuario
    );

    $jwt = new JWT($data);
    return $jwt->toString();
}

$app->run();