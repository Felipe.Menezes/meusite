<?php

include "../../utils/evoxx_autoload.php";
include "../GeoController.php";

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

header("Content-type: application/json;charset=utf-8;");

$app = new \Slim\App();

$app->get("/{idEstado}", 'getByEstado');
$app->get("/{idEstado}/{idCidade}", 'getByCidade');

function getByEstado(Request $request, Response $response, $args) {
  $where = " id_estado = $args[idEstado] ";

  $sql = "SELECT id,
                 id_cidade,
                 id_estado,
                 nome,
                 cep,
                 logradouro,
                 numero,
                 bairro,
                 complemento,
                 telefone,
                 celular,
                 email                 
            FROM assistencias_distribuidores
           WHERE $where
             AND is_assistencia = TRUE
             AND ativo = TRUE
        ORDER BY nome ASC";

  try{

    $st = Conexao::getConnection()->prepare($sql);
    $st->execute();
    $resultado = $st->fetchAll(PDO::FETCH_ASSOC);

    foreach($resultado as $key => $item){
      $resultado[$key]['cidade'] = GeoController::getCidadeById($item['id_cidade'])->name;
      $resultado[$key]['estado'] = GeoController::getEstadoById($item['id_estado'])->name;
    }

    $response->getBody()->write(json_encode($resultado));

  } catch (PDOException $e) {
    ResponseHTTP::error("Erro ao obter as assistências", $e->getMessage());
  }

  return $response;
}

function getByCidade(Request $request, Response $response, $args) {
  $where = " id_estado = $args[idEstado] ";
  $where .= "AND id_cidade = $args[idCidade] ";

  $sql = "SELECT id,
                 id_cidade,
                 id_estado,
                 nome,
                 cep,
                 logradouro,
                 numero,
                 bairro,
                 complemento,
                 telefone,
                 celular,
                 email                 
            FROM assistencias_distribuidores
           WHERE $where
             AND is_distribuidor = TRUE
             AND ativo = TRUE
        ORDER BY nome ASC";

  try{

    $st = Conexao::getConnection()->prepare($sql);
    $st->execute();
    $resultado = $st->fetchAll(PDO::FETCH_ASSOC);

    foreach($resultado as $key => $item){
      $resultado[$key]['cidade'] = GeoController::getCidadeById($item['id_cidade'])->name;
      $resultado[$key]['estado'] = GeoController::getEstadoById($item['id_estado'])->name;
    }

    $response->getBody()->write(json_encode($resultado));

  } catch (PDOException $e) {
    ResponseHTTP::error("Erro ao obter as assistências", $e->getMessage());
  }

  return $response;
}

$app->run();
