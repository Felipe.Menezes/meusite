(function(){

    'use strict';

    angular
        .module('evoxx')
        .factory('$distribuidores', DistribuidoresService);

        DistribuidoresService.$inject = ['$http', '$rootScope'];
        function DistribuidoresService($http, $rootScope){
            var urlApi = $rootScope.urlApi + "Distribuidores.php/";
            
            function getByEstado(idEstado){
                return $http.get(urlApi + idEstado.toString());
            }

            function getByCidade(idEstado, idCidade){
                return $http.get(urlApi + idEstado.toString() + "/" + idCidade.toString());
            }

            return {
                getByEstado: getByEstado,
                getByCidade: getByCidade
            }
        }

})();