(function () {

    'use strict';

    angular
        .module("evoxx")
        .factory("$produtos", ProdutosService);

    ProdutosService.$inject = ['$http', '$rootScope'];
    function ProdutosService($http, $rootScope) {
        const urlApi = $rootScope.urlApi + "Produtos.php";

        const getByCategoria = (idCategoria) => $http.get(urlApi + "/buscarPorCategoria/" + idCategoria);
        const getById = (id) => $http.get(urlApi + "/" + id);
        const getAll = () => $http.get(urlApi);

        function getCaracteristicas(idProduto) {
            return $http
                .get(urlApi + "/" + idProduto + "/Caracteristicas");
        }

        function getEspecificacoes(idProduto) {
            return $http
                .get(urlApi + "/" + idProduto + "/Especificacoes");
        }

        return {
            getByCategoria: getByCategoria,
            getById: getById,
            getAll: getAll,
            getCaracteristicas: getCaracteristicas,
            getEspecificacoes: getEspecificacoes
        };
    }

})();