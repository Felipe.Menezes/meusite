(function () {

    'use strict';

    angular
        .module('evoxx')
        .factory('$configuracaoSite', ConfiguracaoSite);

    ConfiguracaoSite.$inject = ["$rootScope", '$http'];
    function ConfiguracaoSite($rootScope, $http) {
        var url = $rootScope.urlApi + "ConfiguracaoSite.php";

        function get(callback) {
            $http
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        callback(response.data);
                    }
                })
                .catch(function (response) {

                });
        }

        return {
            get: get
        }
    }

})();