(function () {

    'use strict';

    angular
        .module('noticias')
        .factory("$noticias", NoticiasService);

    NoticiasService.$inject = ['$http', '$rootScope'];
    function NoticiasService($http, $rootScope) {
        var urlApi = $rootScope.urlApi + "Noticias.php/";

        function getAll(page, limit) {
            var url = urlApi + "?page=" + page + "&limit=" + limit;
            return $http.get(url);
        }

        function get(idNoticia) {
            var url = urlApi + idNoticia.toString();
            return $http.get(url);
        }

        function getForIndex() {
            var url = urlApi + "forIndex";
            return $http.get(url);
        }

        return {
            get: get,
            getAll: getAll,
            getForIndex: getForIndex
        };
    }

})();