(function(){

    'use strict';

    angular
        .module('evoxx')
        .factory('$assistenciaTecnica', AssistenciaService);

        AssistenciaService.$inject = ['$http', '$rootScope'];
        function AssistenciaService($http, $rootScope){
            var urlApi = $rootScope.urlApi + "AssistenciaTecnica.php/";
            
            function getByEstado(idEstado){
                return $http.get(urlApi + idEstado.toString());
            }

            function getByCidade(idEstado, idCidade){
                return $http.get(urlApi + idEstado.toString() + "/" + idCidade.toString());
            }

            return {
                getByEstado: getByEstado,
                getByCidade: getByCidade
            }
        }

})();