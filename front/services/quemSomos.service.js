(function () {
    'use strict';

    angular
        .module('evoxx')
        .factory('$quemSomos', QuemSomosService);

    QuemSomosService.$inject = ['$http', '$rootScope'];
    function QuemSomosService($http, $rootScope) {
        return {
            get: () => $http.get($rootScope.urlApi + "QuemSomos.php")
        }
    }
})();