(function () {

    'use strict';

    angular
        .module("evoxx")
        .factory('$newsletter', Newsletter);

    Newsletter.$inject = ['$http', '$rootScope'];
    function Newsletter($http, $rootScope) {
        var urlApi = $rootScope.urlApi + "Newsletter.php/registrar";

        function registrar(email, callback, error) {
            if (!email) {
                console.error("#NewsletterService: Email was empty!");
                return;
            }
            var data = {
                email: email
            };
            var post = $http.post(urlApi, data);

            post.then(function (response) {
                if (response.status === 200)
                    callback(response.data);
                else
                    error(response.data.response);
            });

            post.catch(function (response) {
                error(response.data.response);
            });
        }

        return {
            registrar: registrar
        };
    }

})();