(function () {
	'use strict';

	angular
		.module("evoxx")
		.factory("$produtoCategorias", CategoriasService);

	CategoriasService.$inject = ['$http', '$rootScope'];
	function CategoriasService($http, $rootScope) {
		var urlApi = $rootScope.urlApi + "ProdutosCategorias.php"

		var getById = (id_categoria) => $http.get(urlApi + "/" + id_categoria);
		var getAll = () => $http.get(urlApi);


		return {
			getById: getById,
			getAll: getAll
		}

	}
})();