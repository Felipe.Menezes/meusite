(function () {

    'use strict';

    var windowLoaded = false;

    angular
        .module("evoxx", [
            'ngRoute',
            'ngSanitize',
            'contato',
            'distribuidores',
            'assistencia',
            'noticias',
            'produtos',
            'quemSomos',
            'revendedor',
            'acessorestrito',
            'sejaassistencia'            
        ])
        .config(ConfigRoute)
        .run(run);

    run.$inject = ['$rootScope'];
    function run($rootScope) {
        $rootScope.urlApi = "http://api.evoxx.com.br/site/controllers/";

        $rootScope.$on("$routeChangeStart", function (args) {

        });

        $rootScope.$on("$routeChangeSuccess", function (args) {

        });
    }

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider.otherwise({ redirectTo: '/' });
    }

    $(window).on("load", function () {
        windowLoaded = true;
    });
})();