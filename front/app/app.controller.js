(function () {

    'use strict';

    angular
        .module('evoxx')
        .controller("AppController", AppController);

    AppController.$inject = ['$scope', '$newsletter', '$location', '$produtoCategorias'];
    function AppController($scope, $newsletter, $location, $produtoCategorias) {
        $scope.registrar = registrar;
        $scope.estaNaIndex = estaNaIndex;
        $scope.categoriasProdutos = [];

        $produtoCategorias.getAll().then(res => $scope.categoriasProdutos = res.data);

        function estaNaIndex() {
            return ($location.path() === "/");
        }

        function registrar(email) {
            if (!email) {
                console.error("Email was empty");
                return;
            }

            $newsletter.registrar(email, success, erro);

            function success(res) {
                alert("Cadastrado com sucesso!");
                resetMailField();
            }

            function erro(res) {
                alert(res);
                resetMailField();
            }

            function resetMailField() {
                $scope.newsletter.email = "";
            }
        }

    }

})();