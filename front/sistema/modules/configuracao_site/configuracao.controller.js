(function () {

    'use strict';

    angular
        .module('configuracaoSite')
        .controller("ConfiguracaoSiteController", ConfiguracaoSiteController);

    ConfiguracaoSiteController.$inject = ['$scope', '$configuracaoSite'];
    function ConfiguracaoSiteController($scope, $configuracaoSite) {
        $scope.configuracao = {};
        $configuracaoSite.get(function (res) {
            $scope.configuracao.conhecaNossosProdutos = res.conheca_nossos_produtos;
        });

        $scope.salvar = salvar;
        var salvarButton = $("#salvar");

        function salvar(configuracao) {
            salvarButton.text("Salvando...");
            salvarButton.attr("disabled", "disabled");
            
            $configuracaoSite.put(configuracao, function (response) {
                salvarButton.removeClass("btn-primary");
                salvarButton.addClass("btn-success");
                salvarButton.removeAttr("disabled");
                salvarButton.text("Salvo");
                setTimeout(function () {
                    salvarButton.removeClass("btn-success");
                    salvarButton.addClass("btn-primary");
                    salvarButton.text("Salvar");
                }, 3000);
            });
        }
    }
})();