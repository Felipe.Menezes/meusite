(function () {

    'use strict';

    angular
        .module('configuracaoSite')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/configuracaoSite", {
                templateUrl: "modules/configuracao_site/configuracao.html",
                controller: "ConfiguracaoSiteController"
            });
    }
})();