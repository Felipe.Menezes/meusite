(function () {

    'use strict';

    angular
        .module('assistenciasDistribuidores')
        .controller('CadastrarAssistenciasDistribuidoresController', CadastrarAssistenciasDistribuidoresController);

    CadastrarAssistenciasDistribuidoresController.$inject = ['$scope', '$assistenciasDistribuidores', '$geo'];
    function CadastrarAssistenciasDistribuidoresController($scope, $assistenciasDistribuidores, $geo) {
        const salvarButton = $("#salvar");

        $scope.titulo = "Nova(o) Assistência/Distribuidor";
        $scope.categoria = {};

        $scope.estados = [];
        $scope.cidades = [];

        $scope.modelo = {};
        $scope.modelo.is_distribuidor = false;
        $scope.modelo.is_assistencia = false;

        $scope.salvar = salvar;
        $scope.carregarCidades = carregarCidades;

        function salvar(modelo) {
            if (!modeloValida(modelo))
                return;

            salvarButton.text("Salvando...");
            salvarButton.attr("disabled", "disabled");

            $assistenciasDistribuidores
                .cadastrar(modelo)
                .then((response) => {
                    salvarButton.removeClass("btn-primary");
                    salvarButton.addClass("btn-success");
                    salvarButton.removeAttr("disabled");
                    salvarButton.text("Salvo");
                    $timeout(function () {
                        $location.path("/assistencias-distribuidores");
                    }, 1000);

                })
                .catch((response) => {
                    alert("Erro ao salvar!");
                    console.error(response);
                });
        }

        function modeloValida(modelo) {
            if (!modelo.id_estado) {
                alert("Selecione um estado");
                return false;
            }
            if (!modelo.id_cidade) {
                alert("Selecione uma cidade");
                return false;
            }
            if (!modelo.nome) {
                alert("Digite um nome");
                return false;
            }
            if (!modelo.cep) {
                alert("Digite um cep");
                return false;
            }
            if (!modelo.logradouro) {
                alert("Digite um logradouro");
                return false;
            }
            if (!modelo.numero) {
                alert("Digite um número");
                return false;
            }
            if (!modelo.bairro) {
                alert("Digite um bairro");
                return false;
            }
            if (!modelo.telefone) {
                alert("Digite um telefone");
                return false;
            }
            if (!modelo.email) {
                alert("Digite um e-mail");
                return false;
            }
            if (!modelo.is_distribuidor && !modelo.is_assistencia) {
                alert("Selecione ao menos um tipo");
                return false;
            }


            return true;
        }

        function carregarCidades(idEstado) {
            $geo
                .getCidades(idEstado)
                .then(res => $scope.cidades = res.data)
                .catch((res) => {
                    alert("Falha ao carregar cidades");
                    console.error(res);
                });
        }

        $geo
            .getEstados()
            .then((res) => $scope.estados = res.data)
            .catch((res) => {
                alert("Falha ao carregar estados");
                console.error(res);
            });
    }

})();