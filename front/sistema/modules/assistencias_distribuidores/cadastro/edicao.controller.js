(function () {

    'use strict';

    angular
        .module('assistenciasDistribuidores')
        .controller('EditarAssistenciasDistribuidoresController', EditarAssistenciasDistribuidoresController);

    EditarAssistenciasDistribuidoresController.$inject = ['$scope', '$assistenciasDistribuidores'];
    function EditarAssistenciasDistribuidoresController($scope, $assistenciasDistribuidores) {
        $scope.titulo = "Nova(o) Assistência/Distribuidor";
        $scope.categoria = {};
        $scope.salvar = salvar;
        const salvarButton = $("#salvar");

        function salvar(modelo) {
            if (!modeloValida(modelo))
                return;

            salvarButton.text("Salvando...");
            salvarButton.attr("disabled", "disabled");

            $assistenciasDistribuidores
                .put(modelo)
                .then((response) => {
                    salvarButton.removeClass("btn-primary");
                    salvarButton.addClass("btn-success");
                    salvarButton.removeAttr("disabled");
                    salvarButton.text("Salvo");
                    $timeout(function () {
                        $location.path("/assistencias-distribuidores");
                    }, 1000);

                })
                .catch((response) => {
                    alert("Erro ao salvar!");
                    console.error(response);
                });
        }

        function modeloValida(modelo) {
            if (!modelo.nome) {
                alert("Defina uma descrição para a característica");
                return false;
            }
            return true;
        }
    }

})();