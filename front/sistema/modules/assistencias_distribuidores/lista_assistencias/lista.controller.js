(function () {

    'use strict';

    angular
        .module('assistenciasDistribuidores')
        .controller('ListaAssistenciasController', ListaAssistenciasController);

    ListaAssistenciasController.$inject = ['$scope', '$assistenciasDistribuidores'];
    function ListaAssistenciasController($scope, $assistenciasDistribuidores) {
        const isPage = (page) => $scope.page === page;
        const limit = 10;

        $scope.setPage = setPage;
        $scope.isPage = isPage;
        $scope.confirmarExclusao = confirmarExclusao;
        $scope.goTo = goTo;

        $scope.pages = [];
        $scope.listaAssistencias = [];

        $scope.page = 1;
        $scope.numberOfPages = 1;
        $scope.totalRegistros = 0;

        function setPage(page) {
            $scope.page = page;
            loadLista();
            calculateRanges();
        }

        function goTo(path){
            $location.path(path);
        }

        function loadLista() {
            $assistenciasDistribuidores
                .getAllAssistencias($scope.page, limit)
                .then((response) => {
                    $scope.listaAssistencias = response.data.data;
                    $scope.totalRegistros = response.data.totalRows;
                    $scope.numberOfPages = response.data.numberOfPages;
                    loadPages($scope.numberOfPages);
                    calculateRanges();
                })
                .catch((res) => {
                    alert("Erro ao carregar");
                    console.error(res);
                })
        }

        function loadPages(total) {
            $scope.pages = [];
            for (let i = 0; i < total; i++)
                $scope.pages.push(parseInt(i) + 1);
        }

        function calculateRanges() {
            $scope.rangeMin = (($scope.page - 1) * limit) + 1;
            $scope.rangeMax = $scope.page * limit;
            if ($scope.rangeMax > $scope.totalRegistros)
                $scope.rangeMax = $scope.totalRegistros;
        }

        function confirmarExclusao(distribuidor, ev) {

            const confirm = $mdDialog.confirm()
                .title(distribuidor.nome)
                .textContent('Deseja realmente excluir?')
                .ariaLabel('Exclusão Produto')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');

            const apagando = $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Aguarde')
                .textContent('Apagando Distribuidor')
                .ariaLabel('Apagando')
                .targetEvent(ev);

            const show = $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Sucesso')
                .textContent('Apagado com sucesso')
                .ariaLabel('Apagado com Sucesso')
                .ok('OK')
                .targetEvent(ev);

            $mdDialog.show(confirm).then(function () {

                $mdDialog.show(apagando);
                $produtos
                    .excluir(noticia.id)
                    .then((response) => {

                        $mdDialog.hide(apagando);
                        $mdDialog.show(show).then(function () {
                            $window.location.reload();
                        });

                    });
            }, () => {
            });
        }

        loadLista();

    }

})();