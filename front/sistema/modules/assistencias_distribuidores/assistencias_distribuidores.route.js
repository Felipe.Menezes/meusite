(function () {

    'use strict';

    angular
        .module('assistenciasDistribuidores')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/assistencias", {
                templateUrl: 'modules/assistencias_distribuidores/lista_assistencias/lista.html',
                controller: 'ListaAssistenciasController'
            })
            .when("/distribuidores", {
                templateUrl: 'modules/assistencias_distribuidores/lista_distribuidores/lista.html',
                controller: 'ListaDistribuidoresController'
            })
            .when("/assistencias-distribuidores", {
                templateUrl: 'modules/assistencias_distribuidores/lista_ambos/lista.html',
                controller: 'ListaAssistenciasDistribuidoresController'
            })
            .when("/assistencias-distribuidores/cadastrar", {
                templateUrl: 'modules/assistencias_distribuidores/cadastro/cadastro.html',
                controller: 'CadastrarAssistenciasDistribuidoresController'
            })
            .when("/assistencias-distribuidores/editar/{id}", {
                templateUrl: 'modules/assistencias_distribuidores/cadastro/cadastro.html',
                controller: 'EditarAssistenciasDistribuidoresController'
            });
    }

})();