(function () {

    'use strict';

    angular
        .module('noticias')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/noticias", {
                templateUrl: 'modules/noticias/lista/lista.html',
                controller: 'ListaNoticiasController'
            })
            .when("/noticias/cadastrar", {
                templateUrl: 'modules/noticias/cadastro/cadastro.html',
                controller: 'CadastroNoticiasController'
            })
            .when("/noticias/editar/:id", {
                templateUrl: 'modules/noticias/cadastro/cadastro.html',
                controller: 'EditarNoticiasController'
            });
    }

})();