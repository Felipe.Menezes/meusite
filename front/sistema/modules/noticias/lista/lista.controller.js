(function () {

    'use strict';

    angular
        .module('noticias')
        .controller('ListaNoticiasController', ListaNoticiasController);

    ListaNoticiasController.$inject = ['$scope', '$noticias', '$mdDialog', '$location', '$window'];
    function ListaNoticiasController($scope, $noticias, $mdDialog, $location, $window) {
        var limit = 10

        $scope.listaNoticias = [];
        $scope.page = 1;
        $scope.setPage = setPage;
        $scope.isPage = isPage;
        $scope.totalRegistros = 0;
        $scope.numberOfPages = 1;
        $scope.pages = [];
        $scope.confirmarExclusao = confirmarExclusao;

        function loadLista() {
            $scope.listaNoticias = $noticias.getListagem({
                page: $scope.page,
                then: function (response) {
                    $scope.listaNoticias = response.data;
                    $scope.totalRegistros = response.totalRows;
                    $scope.numberOfPages = response.numberOfPages;
                    loadPages($scope.numberOfPages);
                    calculateRanges();
                }
            });
        }

        function confirmarExclusao(noticia, ev) {

            var confirm = $mdDialog.confirm()
                .title(noticia.titulo)
                .textContent('Deseja realmente excluir?')
                .ariaLabel('Exclusão notícia')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');

            var apagando = $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Aguarde')
                .textContent('Apagando noticia')
                .ariaLabel('Apagando')
                .targetEvent(ev);

            var show = $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Sucesso')
                .textContent('Apagado com sucesso')
                .ariaLabel('Apagado com Sucesso')
                .ok('OK')
                .targetEvent(ev);

            $mdDialog.show(confirm).then(function () {

                $mdDialog.show(apagando);
                $noticias
                    .excluir(noticia.id)
                    .then(function (response) {

                        $mdDialog.hide(apagando);
                        $mdDialog.show(show).then(function () {
                            $window.location.reload();
                        });

                    });
            }, function () { });
        };

        function loadPages(total) {
            $scope.pages = [];
            for (var i = 0; i < total; i++)
                $scope.pages.push(parseInt(i) + 1);
        }

        function calculateRanges() {
            $scope.rangeMin = (($scope.page - 1) * limit) + 1;
            $scope.rangeMax = $scope.page * limit;
            if ($scope.rangeMax > $scope.totalRegistros)
                $scope.rangeMax = $scope.totalRegistros;
        }

        function setPage(page) {
            $scope.page = page;
            loadLista();
            calculateRanges();
        }

        function isPage(page) {
            return $scope.page === page;
        }

        (function () {
            loadLista();
        })();
    }

})();