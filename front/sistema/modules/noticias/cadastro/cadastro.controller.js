(function () {

    'use strict';

    angular
        .module('noticias')
        .controller('CadastroNoticiasController', CadastroNoticiasController);

    CadastroNoticiasController.$inject = ['$scope', '$noticias', '$location', '$timeout'];
    function CadastroNoticiasController($scope, $noticias, $location, $timeout) {
        $scope.titulo = "Nova notícia";
        $scope.noticia = {};
        $scope.novasImagens = [];
        $scope.salvar = salvar;
        $scope.noticia.data_publicacao = formatDate(new Date());
        var salvarButton = $("#salvar");

        function salvar(noticia) {
            if (!noticiaValida(noticia))
                return;

            salvarButton.text("Salvando...");
            salvarButton.attr("disabled", "disabled");

            $noticias.cadastrar(noticia, $scope.novasImagens)
                .then(function (response) {
                    salvarButton.removeClass("btn-primary");
                    salvarButton.addClass("btn-success");
                    salvarButton.removeAttr("disabled");
                    salvarButton.text("Salvo");
                    $timeout(function () {
                        $location.path("/noticias");
                    }, 1000);

                }).catch(function (response) {
                alert("Erro ao salvar!");
                console.error(response);
            });
        }

        function noticiaValida(noticia) {
            if (!noticia.data_publicacao) {
                alert("Defina uma data para a publicação");
                return false;
            }
            if (!noticia.titulo) {
                alert("Defina um título para a publicação");
                return false;
            }
            if (!noticia.artigo) {
                alert("Escreva um artigo para a publicação");
                return false;
            }
            return true;
        }

        $(document).ready(initializeComponents);
    }

    function initializeComponents() {

        $("[data-type='datetime']").mask("00/00/0000 00:00:00");

        const fileInputTextDiv = document.getElementById('file_input_text_div');
        const fileInput = document.getElementById('file_input_file');
        const fileInputText = document.getElementById('file_input_text');

        fileInput.addEventListener('change', changeInputText);
        fileInput.addEventListener('change', changeState);

        function changeInputText(evt) {
            const qtdFiles = evt.target.files.length;
            const str = fileInput.value;

            if (qtdFiles > 1) {
                fileInputText.value = qtdFiles.toString() + " imagens";
            } else {
                let i;
                if (str.lastIndexOf('\\')) {
                    i = str.lastIndexOf('\\') + 1;
                } else if (str.lastIndexOf('/')) {
                    i = str.lastIndexOf('/') + 1;
                }
                fileInputText.value = str.slice(i, str.length);
            }
        }

        function changeState(evt) {
            if (fileInputText.value.length !== 0) {
                if (!fileInputTextDiv.classList.contains("is-focused")) {
                    fileInputTextDiv.classList.add('is-focused');
                }
            } else {
                if (fileInputTextDiv.classList.contains("is-focused")) {
                    fileInputTextDiv.classList.remove('is-focused');
                }
            }
        }

    }

    function addZero(num) {
        return (num >= 0 && num < 10) ? "0" + num : num;
    }

    function formatDate(date) {
        var day = addZero(date.getDate());
        var month = addZero(date.getMonth());
        var year = addZero(date.getFullYear());
        var hour = addZero(date.getHours());
        var minutes = addZero(date.getMinutes());
        var seconds = addZero(date.getSeconds());

        return day + "/" + month + "/" + year + " " + hour + ":" + minutes + ":" + seconds;
    }

})();