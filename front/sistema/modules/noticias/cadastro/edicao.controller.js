(function () {

    'use strict';

    angular
        .module('noticias')
        .controller('EditarNoticiasController', EditarNoticiasController);

    EditarNoticiasController.$inject = ['$scope', '$routeParams', '$noticias', '$location', '$timeout', '$mdDialog'];
    function EditarNoticiasController($scope, $routeParams, $noticias, $location, $timeout, $mdDialog) {
        $scope.titulo = "Alterar notícia #" + $routeParams.id;
        $scope.noticia = {};
        $scope.salvar = salvar;
        $scope.novasImagens = [];
        $scope.deletarFoto = deletarFoto;
        const salvarButton = $("#salvar");

        let noticiaSalva = false;
        let fotosSalva = false;

        function salvar(noticia) {
            if (!noticiaValida(noticia))
                return;

            noticiaSalva = false;
            fotosSalva = false;

            salvarButton.text("Salvando...");
            salvarButton.attr("disabled", "disabled");

            $noticias.putNoticia($routeParams.id, noticia)
                .then(function (response) {
                    noticiaSalva = true;
                    finish(true);
                })
                .catch(function (response) {
                    console.error(response.data);
                    noticiaSalva = true;
                    finish(false);
                });

            if ($scope.novasImagens.length)
                $noticias.adicionarFotos($routeParams.id, $scope.novasImagens)
                    .then(function (response) {
                        fotosSalva = true;
                        finish(true);
                    })
                    .catch(function (response) {
                        console.error(response.data);
                        fotosSalva = true;
                        finish(false);
                    });

        }

        function deletarFoto(idFoto, ev) {
            const confirm = $mdDialog.confirm()
                .title("Exclusão")
                .textContent('Deseja realmente excluir?')
                .ariaLabel('Exclusão foto')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');

            const apagando = $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Aguarde')
                .textContent('Apagando foto')
                .ariaLabel('Apagando')
                .targetEvent(ev);

            const show = $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Sucesso')
                .textContent('Apagado com sucesso')
                .ariaLabel('Apagado com Sucesso')
                .ok('OK')
                .targetEvent(ev);

            $mdDialog
                .show(confirm)
                .then(() => {

                    $mdDialog.show(apagando);

                    $noticias
                        .removerFoto(idFoto)
                        .then((res) => {
                            $scope.noticia.fotos = $scope.noticia.fotos.filter((i) => {
                                return (i.id !== idFoto);
                            });

                            $mdDialog.hide(apagando);
                            $mdDialog.show(show);
                        });

                })
                .catch(() => {
                });
        }

        function finish(sucesso) {
            if (!fotosSalva || !noticiaSalva)
                return;

            if (sucesso) {
                salvarButton.removeClass("btn-primary");
                salvarButton.addClass("btn-success");
                salvarButton.removeAttr("disabled");
                salvarButton.text("Salvo");
                $timeout(function () {
                    salvarButton.removeClass("btn-success");
                    salvarButton.addClass("btn-primary");
                    salvarButton.text("Salvar");
                    $location.path("/noticias");
                }, 1000);
            } else {

                salvarButton.removeClass("btn-primary");
                salvarButton.addClass("btn-danger");
                salvarButton.removeAttr("disabled");
                salvarButton.text("Erro");
                setTimeout(function () {
                    salvarButton.removeClass("btn-success");
                    salvarButton.addClass("btn-primary");
                    salvarButton.text("Salvar");
                }, 3000);
            }
        }

        $(document).ready(initializeComponents);

        function load() {
            $noticias
                .getNoticia($routeParams.id, function (response) {
                    $scope.noticia = response;
                }, function (response) {
                    alert("ERRO");
                    console.error(response);
                });
        }

        function noticiaValida(noticia) {
            if (!noticia.data_publicacao) {
                alert("Defina uma data para a publicação");
                return false;
            }
            if (!noticia.titulo) {
                alert("Defina um título para a publicação");
                return false;
            }
            if (!noticia.artigo) {
                alert("Escreva um artigo para a publicação");
                return false;
            }
            return true;
        }

        (function () {
            load();
        })();

    }

    function initializeComponents() {

        $("[data-type='datetime']").mask("00/00/0000 00:00:00");

        const fileInputTextDiv = document.getElementById('file_input_text_div');
        const fileInput = document.getElementById('file_input_file');
        const fileInputText = document.getElementById('file_input_text');

        fileInput.addEventListener('change', changeInputText);
        fileInput.addEventListener('change', changeState);

        function changeInputText(evt) {
            const qtdFiles = evt.target.files.length;
            const str = fileInput.value;

            if (qtdFiles > 1) {
                fileInputText.value = qtdFiles.toString() + " imagens";
            } else {
                let i;
                if (str.lastIndexOf('\\')) {
                    i = str.lastIndexOf('\\') + 1;
                } else if (str.lastIndexOf('/')) {
                    i = str.lastIndexOf('/') + 1;
                }
                fileInputText.value = str.slice(i, str.length);
            }
        }

        function changeState(evt) {
            if (fileInputText.value.length !== 0) {
                if (!fileInputTextDiv.classList.contains("is-focused")) {
                    fileInputTextDiv.classList.add('is-focused');
                }
            } else {
                if (fileInputTextDiv.classList.contains("is-focused")) {
                    fileInputTextDiv.classList.remove('is-focused');
                }
            }
        }

    }

})();