(function () {

    'use strict';

    angular
        .module('produtos')
        .controller('ProdutoEspecificacaoController', ProdutoEspecificacaoController);

    ProdutoEspecificacaoController.$inject = ['$scope', '$routeParams', '$produtos', '$mdDialog'];
    function ProdutoEspecificacaoController($scope, $routeParams, $produtos, $mdDialog) {
        $scope.idProduto = $routeParams.idProduto;
        $scope.listaEspecificacoes = [];
        $scope.novaEspecificacao = {};
        $scope.estaAdicionando = false;
        $scope.adicionarEspecificacao = adicionarEspecificacao;
        $scope.cancelarCadastro = cancelarCadastro;
        $scope.salvarEspecificacao = salvarEspecificacao;
        $scope.confirmarExclusao = confirmarExclusao;

        $produtos
            .getEspecificacoes($scope.idProduto)
            .then((res) => {
                $scope.listaEspecificacoes = res.data;
            })
            .catch((res) => {
                alert("Erro ao carregar as características");
                console.error(res);
            });

        function adicionarEspecificacao() {
            $scope.novaEspecificacao = {};
            $scope.estaAdicionando = true;
        }

        function cancelarCadastro() {
            $scope.novaEspecificacao = {};
            $scope.estaAdicionando = false;
        }

        function salvarEspecificacao(especificacao) {
            if (!validaEspecificacao(especificacao))
                return;

            getButtonSalvar().disable();

            $produtos
                .salvarEspecificacao($scope.idProduto, especificacao)
                .then((res) => {
                    especificacao.id = res.data.id;
                    $scope.listaEspecificacoes.push(angular.copy(especificacao));
                    cancelarCadastro();
                    getButtonSalvar().enable();
                })
                .catch((res) => {
                    alert("Erro ao salvar");
                    console.error(res);
                });
        }

        function confirmarExclusao(especificacao, ev) {
            const confirm = $mdDialog.confirm()
                .title(especificacao.especificacao)
                .textContent('Deseja realmente excluir?')
                .ariaLabel('Exclusão')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');

            const apagando = $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Aguarde')
                .textContent('Apagando Característica')
                .ariaLabel('Apagando')
                .targetEvent(ev);

            const show = $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Sucesso')
                .textContent('Apagado com sucesso')
                .ariaLabel('Apagado com Sucesso')
                .ok('OK')
                .targetEvent(ev);

            $mdDialog.show(confirm).then(() => {

                $mdDialog.show(apagando);
                $produtos
                    .excluirEspecificacao($scope.idProduto, especificacao)
                    .then((res) => {
                        $scope.listaEspecificacoes = $scope.listaEspecificacoes.filter((i) => {
                            return (i.id !== especificacao.id);
                        });
                        $mdDialog.hide(apagando);
                        $mdDialog.show(show);
                    })
                    .catch((res) => {
                        alert("Erro ao excluir");
                        console.error(res);
                    });
            });


        }

        function getButtonSalvar() {
            return $("#salvar");
        }

        function validaEspecificacao(especificacao) {
            if (!especificacao.titulo) {
                alert("Digite um titulo");
                return false;
            }
            if (!especificacao.valor) {
                alert("Digite um valor");
                return false;
            }
            return true;
        }

    }

    $.fn.disable = function () {
        $(this).attr("disabled", "disabled");
    };

    $.fn.enable = function () {
        $(this).removeAttr("disabled");
    };

})();