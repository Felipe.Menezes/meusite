(function () {

    'use strict';

    angular
        .module('produtos')
        .controller('ProdutoCaracteristicaController', ProdutoCaracteristicaController);

    ProdutoCaracteristicaController.$inject = ['$scope', '$routeParams', '$produtosCaracteristica', '$produtos', '$mdDialog'];
    function ProdutoCaracteristicaController($scope, $routeParams, $produtosCaracteristica, $produtos, $mdDialog) {
        $scope.idProduto = $routeParams.idProduto;
        $scope.listaCaracteristicas = [];
        $scope.tiposCaracteristicas = [];
        $scope.novaCaracteristica = {};
        $scope.estaAdicionando = false;
        $scope.adicionarCaracteristica = adicionarCaracteristica;
        $scope.cancelarCadastro = cancelarCadastro;
        $scope.salvarCaracteristica = salvarCaracteristica;
        $scope.confirmarExclusao = confirmarExclusao;

        $produtosCaracteristica.getAll()
            .then((res) => $scope.tiposCaracteristicas = res.data);

        $produtos
            .getCaracteristicas($scope.idProduto)
            .then((res) => {
                $scope.listaCaracteristicas = res.data;
            })
            .catch((res) => {
                alert("Erro ao carregar as características");
                console.error(res);
            });

        function adicionarCaracteristica() {
            $scope.novaCaracteristica = {};
            $scope.estaAdicionando = true;
        }

        function cancelarCadastro() {
            $scope.novaCaracteristica = {};
            $scope.estaAdicionando = false;
        }

        function salvarCaracteristica(caracteristica) {
            if (!validaCaracteristica(caracteristica))
                return;

            getButtonSalvar().disable();

            convertObject(caracteristica);

            $produtos
                .salvarCaracteristica($scope.idProduto, caracteristica)
                .then((res) => {
                    caracteristica.id = res.data.id;
                    $scope.listaCaracteristicas.push(angular.copy(caracteristica));
                    cancelarCadastro();
                    getButtonSalvar().enable();
                })
                .catch((res) => {
                    alert("Erro ao salvar");
                    console.error(res);
                });
        }

        function convertObject(caracteristica) {
            caracteristica.tipoSelect = JSON.parse(caracteristica.tipoSelect);
            caracteristica.id_tipo = caracteristica.tipoSelect.id;
            caracteristica.tipo = caracteristica.tipoSelect.descricao;

            delete caracteristica.tipoSelect;
        }

        function confirmarExclusao(caracteristica, ev) {
            const confirm = $mdDialog.confirm()
                .title(caracteristica.caracteristica)
                .textContent('Deseja realmente excluir?')
                .ariaLabel('Exclusão')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');

            const apagando = $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Aguarde')
                .textContent('Apagando Característica')
                .ariaLabel('Apagando')
                .targetEvent(ev);

            const show = $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Sucesso')
                .textContent('Apagado com sucesso')
                .ariaLabel('Apagado com Sucesso')
                .ok('OK')
                .targetEvent(ev);

            $mdDialog.show(confirm).then(() => {

                $mdDialog.show(apagando);
                $produtos
                    .excluirCaracteristica($scope.idProduto, caracteristica)
                    .then((res) => {
                        $scope.listaCaracteristicas = $scope.listaCaracteristicas.filter((i) => {
                            return (i.id !== caracteristica.id);
                        });
                        $mdDialog.hide(apagando);
                        $mdDialog.show(show);
                    })
                    .catch((res) => {
                        alert("Erro ao excluir");
                        console.error(res);
                    });
            });


        }

        function getButtonSalvar() {
            return $("#salvar");
        }

        function validaCaracteristica(caracteristica) {
            if (!caracteristica.tipoSelect) {
                alert("Selecione um tipo");
                return false;
            }
            if (!caracteristica.caracteristica) {
                alert("Digite uma característica");
                return false;
            }
            return true;
        }

    }

    $.fn.disable = function () {
        $(this).attr("disabled", "disabled");
    };

    $.fn.enable = function () {
        $(this).removeAttr("disabled");
    };

})();