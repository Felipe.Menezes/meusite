(function () {

    'use strict';

    angular
        .module('produtos')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/produtos", {
                templateUrl: 'modules/produtos/lista/lista.html',
                controller: 'ListaProdutosController'
            })
            .when("/produtos/cadastrar", {
                templateUrl: 'modules/produtos/cadastro/cadastro.html',
                controller: 'CadastroProdutosController'
            })
            .when("/produtos/editar/:id", {
                templateUrl: 'modules/produtos/cadastro/cadastro.html',
                controller: 'EditarProdutosController'
            })
            .when("/produtos/editar/:idProduto/caracteristicas", {
                templateUrl: 'modules/produtos/caracteristicas/caracteristicas.html',
                controller: 'ProdutoCaracteristicaController'
            })
            .when("/produtos/editar/:idProduto/especificacoes", {
                templateUrl: 'modules/produtos/especificacoes/especificacoes.html',
                controller: 'ProdutoEspecificacaoController'
            });
    }

})();