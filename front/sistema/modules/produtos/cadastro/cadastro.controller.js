(function () {

    'use strict';

    angular
        .module('produtos')
        .controller('CadastroProdutosController', CadastroProdutosController);

    CadastroProdutosController.$inject = ['$scope', '$produtos', '$location', '$timeout', '$produtosCategoria'];
    function CadastroProdutosController($scope, $produtos, $location, $timeout, $produtosCategoria) {
        const salvarButton = $("#salvar");

        $scope.titulo = "Novo Produto";
        $scope.produto = {};
        $scope.novasImagens = [];
        $scope.novoManual = {};
        $scope.categorias = [];
        $scope.editando = false;
        $scope.salvar = salvar;

        function salvar(produto) {
            if (!produtoValida(produto))
                return;

            salvarButton.text("Salvando...");
            salvarButton.attr("disabled", "disabled");

            $produtos.cadastrar(produto, $scope.novasImagens, $scope.novoManual)
                .then(() => {
                    salvarButton.removeClass("btn-primary");
                    salvarButton.addClass("btn-success");
                    salvarButton.removeAttr("disabled");
                    salvarButton.text("Salvo");
                    $timeout(function () {
                        $location.path("/produtos");
                    }, 1000);

                })
                .catch((response) => {
                    alert("Erro ao salvar!");
                    console.error(response);
                });
        }

        function produtoValida(produto) {
            if (!produto.id_categoria) {
                alert("Selecione uma categoria para o produto!");
                return false;
            }
            if (!produto.descricao) {
                alert("Defina uma descrição para o produto!");
                return false;
            }
            return true;
        }

        function loadCategorias() {
            $produtosCategoria
                .getAll()
                .then((res) => $scope.categorias = res.data)
                .catch((res) => {
                    alert("Erro ao carregas as categorias");
                    console.error(res);
                });
        }

        loadCategorias();
        initializeComponents();
    }

    function initializeComponents() {
        const fileInputTextDivFotos = document.getElementById('file_input_text_div_fotos');
        const fileInputTextDivManual = document.getElementById('file_input_text_div_manual');
        const fileInputFotos = document.getElementById('fotosInputFile');
        const fileInputManual = document.getElementById('manualInputFile');
        const fileInputTextFotos = document.getElementById('fotosInputText');
        const fileInputTextManual = document.getElementById('manualInputText');

        fileInputFotos.addEventListener('change', changeInputTextFotos);
        fileInputFotos.addEventListener('change', changeStateFotos);

        fileInputManual.addEventListener('change', changeInputTextManual);
        fileInputManual.addEventListener('change', changeStateManual);

        function changeInputTextFotos(evt) {
            const qtdFiles = evt.target.files.length;
            const str = fileInputFotos.value;

            if (qtdFiles > 1) {
                fileInputTextFotos.value = qtdFiles.toString() + " imagens";
            } else {
                let i;
                if (str.lastIndexOf('\\')) {
                    i = str.lastIndexOf('\\') + 1;
                } else if (str.lastIndexOf('/')) {
                    i = str.lastIndexOf('/') + 1;
                }
                fileInputTextFotos.value = str.slice(i, str.length);
            }
        }

        function changeStateFotos(evt) {
            if (fileInputTextFotos.value.length !== 0) {
                if (!fileInputTextDivFotos.classList.contains("is-focused")) {
                    fileInputTextDivFotos.classList.add('is-focused');
                }
            } else {
                if (fileInputTextDivFotos.classList.contains("is-focused")) {
                    fileInputTextDivFotos.classList.remove('is-focused');
                }
            }
        }

        function changeInputTextManual(evt) {
            const str = fileInputManual.value;

            let i;
            if (str.lastIndexOf('\\')) {
                i = str.lastIndexOf('\\') + 1;
            } else if (str.lastIndexOf('/')) {
                i = str.lastIndexOf('/') + 1;
            }
            fileInputTextManual.value = str.slice(i, str.length);

        }

        function changeStateManual(evt) {
            if (fileInputTextManual.value.length !== 0) {
                if (!fileInputTextDivManual.classList.contains("is-focused")) {
                    fileInputTextDivManual.classList.add('is-focused');
                }
            } else {
                if (fileInputTextDivManual.classList.contains("is-focused")) {
                    fileInputTextDivManual.classList.remove('is-focused');
                }
            }
        }
    }

})();