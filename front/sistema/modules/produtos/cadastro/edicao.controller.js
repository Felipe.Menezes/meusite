(function () {

    'use strict';

    angular
        .module('produtos')
        .controller('EditarProdutosController', EditarProdutosController);

    EditarProdutosController.$inject = ['$scope', '$routeParams', '$produtos', '$location', '$timeout', '$produtosCategoria', '$mdDialog'];
    function EditarProdutosController($scope, $routeParams, $produtos, $location, $timeout, $produtosCategoria, $mdDialog) {
        const salvarButton = $("#salvar");
        let fotosSalva = false;
        let manualSalvo = false;
        let produtoSalvo = false;

        $scope.titulo = "Alterar Produto #" + $routeParams.id;
        $scope.produto = {};
        $scope.categorias = [];
        $scope.novasImagens = [];
        $scope.editando = true;
        $scope.salvar = salvar;
        $scope.deletarFoto = deletarFoto;

        function salvar(produto) {
            if (!produtoValida(produto))
                return;

            salvarButton.text("Salvando...");
            salvarButton.attr("disabled", "disabled");

            $produtos.putProduto($routeParams.id, produto)
                .then(function (response) {
                    produtoSalvo = true;
                    finish(true);
                })
                .catch(function (response) {
                    console.error(response.data);

                    produtoSalvo = false;
                    finish(false);
                });

            if ($scope.novasImagens.length) {
                $produtos
                    .adicionarFotos($routeParams.id, $scope.novasImagens)
                    .then((res) => {
                        fotosSalva = true;
                        finish(true);
                    })
                    .catch((res) => {
                        alert("Erro ao salvar as fotos");
                        console.error(res);
                        fotosSalva = false;
                        finish(true);
                    })
            } else fotosSalva = true;

            if ($scope.novoManual) {
                $produtos
                    .setManual($routeParams.id, $scope.novoManual)
                    .then((res) => {
                        manualSalvo = true;
                        finish(true);
                    })
                    .catch((res) => {
                        alert("Erro ao salvar o manual");
                        console.error(res);
                        manualSalvo = false;
                        finish(true);
                    })
            } else manualSalvo = true;

        }

        function finish(sucesso) {
            if (!fotosSalva || !produtoSalvo || !manualSalvo)
                return;

            if (sucesso) {
                salvarButton.removeClass("btn-primary");
                salvarButton.addClass("btn-success");
                salvarButton.removeAttr("disabled");
                salvarButton.text("Salvo");
                $timeout(function () {
                    salvarButton.removeClass("btn-success");
                    salvarButton.addClass("btn-primary");
                    salvarButton.text("Salvar");
                    $location.path("/produtos");
                }, 1000);
            } else {

                salvarButton.removeClass("btn-primary");
                salvarButton.addClass("btn-danger");
                salvarButton.removeAttr("disabled");
                salvarButton.text("Erro");
                $timeout(function () {
                    salvarButton.removeClass("btn-success");
                    salvarButton.addClass("btn-primary");
                    salvarButton.text("Salvar");
                }, 3000);
            }
        }

        function loadProduto() {
            $produtos
                .getProduto($routeParams.id)
                .then((response) => {
                    $scope.produto = response.data;
                })
                .catch((response) => {
                    alert("Erro ao carregar o produto");
                    console.error(response);
                });
        }

        function produtoValida(produto) {
            if (!produto.id_categoria) {
                alert("Selecione uma categoria para o produto");
                return false;
            }
            if (!produto.descricao) {
                alert("Defina uma descrição para o produto");
                return false;
            }
            return true;
        }

        function loadCategorias() {
            $produtosCategoria
                .getAll()
                .then((res) => $scope.categorias = res.data)
                .catch((res) => {
                    alert("Erro ao carregas as categorias");
                    console.error(res);
                });
        }

        function deletarFoto(idFoto, ev) {
            const confirm = $mdDialog.confirm()
                .title("Exclusão")
                .textContent('Deseja realmente excluir?')
                .ariaLabel('Exclusão foto')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');

            const apagando = $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Aguarde')
                .textContent('Apagando foto')
                .ariaLabel('Apagando')
                .targetEvent(ev);

            const show = $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Sucesso')
                .textContent('Apagado com sucesso')
                .ariaLabel('Apagado com Sucesso')
                .ok('OK')
                .targetEvent(ev);

            $mdDialog
                .show(confirm)
                .then(() => {

                    $mdDialog.show(apagando);

                    $produtos
                        .removerFoto(idFoto)
                        .then((res) => {
                            $scope.produto.fotos = $scope.produto.fotos.filter((i) => {
                                return (i.id !== idFoto);
                            });

                            $mdDialog.hide(apagando);
                            $mdDialog.show(show);
                        });

                })
                .catch(() => {
                });
        }

        loadCategorias();
        loadProduto();
        initializeComponents();
    }

    function initializeComponents() {
        const fileInputTextDivFotos = document.getElementById('file_input_text_div_fotos');
        const fileInputTextDivManual = document.getElementById('file_input_text_div_manual');
        const fileInputFotos = document.getElementById('fotosInputFile');
        const fileInputManual = document.getElementById('manualInputFile');
        const fileInputTextFotos = document.getElementById('fotosInputText');
        const fileInputTextManual = document.getElementById('manualInputText');

        fileInputFotos.addEventListener('change', changeInputTextFotos);
        fileInputFotos.addEventListener('change', changeStateFotos);

        fileInputManual.addEventListener('change', changeInputTextManual);
        fileInputManual.addEventListener('change', changeStateManual);

        function changeInputTextFotos(evt) {
            const qtdFiles = evt.target.files.length;
            const str = fileInputFotos.value;

            if (qtdFiles > 1) {
                fileInputTextFotos.value = qtdFiles.toString() + " imagens";
            } else {
                let i;
                if (str.lastIndexOf('\\')) {
                    i = str.lastIndexOf('\\') + 1;
                } else if (str.lastIndexOf('/')) {
                    i = str.lastIndexOf('/') + 1;
                }
                fileInputTextFotos.value = str.slice(i, str.length);
            }
        }

        function changeStateFotos(evt) {
            if (fileInputTextFotos.value.length !== 0) {
                if (!fileInputTextDivFotos.classList.contains("is-focused")) {
                    fileInputTextDivFotos.classList.add('is-focused');
                }
            } else {
                if (fileInputTextDivFotos.classList.contains("is-focused")) {
                    fileInputTextDivFotos.classList.remove('is-focused');
                }
            }
        }

        function changeInputTextManual(evt) {
            const str = fileInputManual.value;

            let i;
            if (str.lastIndexOf('\\')) {
                i = str.lastIndexOf('\\') + 1;
            } else if (str.lastIndexOf('/')) {
                i = str.lastIndexOf('/') + 1;
            }
            fileInputTextManual.value = str.slice(i, str.length);

        }

        function changeStateManual(evt) {
            if (fileInputTextManual.value.length !== 0) {
                if (!fileInputTextDivManual.classList.contains("is-focused")) {
                    fileInputTextDivManual.classList.add('is-focused');
                }
            } else {
                if (fileInputTextDivManual.classList.contains("is-focused")) {
                    fileInputTextDivManual.classList.remove('is-focused');
                }
            }
        }
    }

})();