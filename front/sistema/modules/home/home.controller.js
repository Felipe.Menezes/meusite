(function () {

    'use strict';

    angular
        .module('home')
        .controller("HomeController", HomeController);

    HomeController.$inject = ['$scope'];
    function HomeController($scope) {
        $scope.oi = "I";
    }

})();