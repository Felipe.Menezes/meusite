(function () {

    'use strict';

    angular
        .module('home')
        .config(configHome);

    configHome.$inject = ['$routeProvider'];

    function configHome($routeProvider) {
        $routeProvider
            .when("/home", {
                templateUrl: 'modules/home/home.html',
                controller: 'HomeController'
            });
    }

})();