(function () {

    'use strict';

    angular
        .module('produtos_categoria')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/produtos-categoria", {
                templateUrl: 'modules/produtos_categoria/lista/lista.html',
                controller: 'ListaProdutosCategoriaController'
            })
            .when("/produtos-categoria/cadastrar", {
                templateUrl: 'modules/produtos_categoria/cadastro/cadastro.html',
                controller: 'CadastroProdutosCategoriaController'
            })
            .when("/produtos-categoria/editar/:id", {
                templateUrl: 'modules/produtos_categoria/cadastro/cadastro.html',
                controller: 'EditarProdutosCategoriaController'
            });
    }

})();