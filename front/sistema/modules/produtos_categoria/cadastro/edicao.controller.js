(function () {

    'use strict';

    angular
        .module('produtos_categoria')
        .controller('EditarProdutosCategoriaController', EditarProdutosCategoriaController);

    EditarProdutosCategoriaController.$inject = ['$scope', '$routeParams', '$produtosCategoria', '$location', '$timeout'];
    function EditarProdutosCategoriaController($scope, $routeParams, $produtosCategoria, $location, $timeout) {
        $scope.titulo = "Alterar Categoria #" + $routeParams.id;
        $scope.categoria = {};
        $scope.salvar = salvar;
        const salvarButton = $("#salvar");

        function salvar(categoria) {
            if (!produtoValida(categoria))
                return;

            salvarButton.text("Salvando...");
            salvarButton.attr("disabled", "disabled");

            $produtosCategoria
                .putCategoria($routeParams.id, categoria)
                .then(function (response) {
                    salvarButton.removeClass("btn-primary");
                    salvarButton.addClass("btn-success");
                    salvarButton.removeAttr("disabled");
                    salvarButton.text("Salvo");
                    $timeout(function () {
                        salvarButton.removeClass("btn-success");
                        salvarButton.addClass("btn-primary");
                        salvarButton.text("Salvar");
                        $location.path("/produtos-categoria");
                    }, 1000);
                })
                .catch(function (response) {
                    console.error(response.data);

                    salvarButton.removeClass("btn-primary");
                    salvarButton.addClass("btn-danger");
                    salvarButton.removeAttr("disabled");
                    salvarButton.text("Erro");
                    setTimeout(function () {
                        salvarButton.removeClass("btn-success");
                        salvarButton.addClass("btn-primary");
                        salvarButton.text("Salvar");
                    }, 3000);
                });

        }

        function load() {
            $produtosCategoria
                .getCategoria($routeParams.id)
                .then(function (response) {
                    $scope.categoria = response.data;
                })
                .catch(function (response) {
                    alert("ERRO");
                    console.error(response);
                });
        }

        function produtoValida(categoria) {
            if (!categoria.descricao) {
                alert("Defina uma descrição para a categoria");
                return false;
            }
            return true;
        }

        (function () {
            load();
        })();

    }

})();