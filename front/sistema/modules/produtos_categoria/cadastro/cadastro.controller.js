(function () {

    'use strict';

    angular
        .module('produtos_categoria')
        .controller('CadastroProdutosCategoriaController', CadastroProdutosCategoriaController);

    CadastroProdutosCategoriaController.$inject = ['$scope', '$produtosCategoria', '$location', '$timeout'];
    function CadastroProdutosCategoriaController($scope, $produtosCategoria, $location, $timeout) {
        $scope.titulo = "Nova Categoria";
        $scope.categoria = {};
        $scope.salvar = salvar;
        const salvarButton = $("#salvar");

        function salvar(categoria) {
            if (!categoriaValida(categoria))
                return;

            salvarButton.text("Salvando...");
            salvarButton.attr("disabled", "disabled");

            $produtosCategoria
                .cadastrar(categoria)
                .then((response) => {
                    salvarButton.removeClass("btn-primary");
                    salvarButton.addClass("btn-success");
                    salvarButton.removeAttr("disabled");
                    salvarButton.text("Salvo");
                    $timeout(function () {
                        $location.path("/produtos-categoria");
                    }, 1000);

                })
                .catch((response) => {
                    alert("Erro ao salvar!");
                    console.error(response);
                });
        }

        function categoriaValida(categoria) {
            if (!categoria.descricao) {
                alert("Defina uma descrição para a categoria");
                return false;
            }
            return true;
        }

    }

})();