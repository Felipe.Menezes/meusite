(function () {

    'use strict';

    angular
        .module('quemSomos')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/quemSomos", {
                templateUrl: "modules/quem_somos/quemSomos.html",
                controller: "QuemSomosController"
            });
    }
})();