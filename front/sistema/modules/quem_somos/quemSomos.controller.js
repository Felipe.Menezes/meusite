(function () {

    'use strict';

    angular
        .module('quemSomos')
        .controller("QuemSomosController", QuemSomosController);

    QuemSomosController.$inject = ['$scope', '$quemSomos'];

    function QuemSomosController($scope, $quemSomos) {
        $scope.data = {};
        $quemSomos.get().then((res) => $scope.data = res.data);

        $scope.salvar = salvar;
        const salvarButton = $("#salvar");

        function salvar(data) {
            salvarButton.text("Salvando...");
            salvarButton.attr("disabled", "disabled");

            $quemSomos.put(data).then((response) => {
                salvarButton.removeClass("btn-primary");
                salvarButton.addClass("btn-success");
                salvarButton.removeAttr("disabled");
                salvarButton.text("Salvo");
                setTimeout(function () {
                    salvarButton.removeClass("btn-success");
                    salvarButton.addClass("btn-primary");
                    salvarButton.text("Salvar");
                }, 3000);
            });
        }
    }
})();