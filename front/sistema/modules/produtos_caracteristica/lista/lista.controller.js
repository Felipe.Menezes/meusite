(function () {

    'use strict';

    angular
        .module('produtos_categoria')
        .controller('ListaProdutosCaracteristicaController', ListaProdutosCaracteristicaController);

    ListaProdutosCaracteristicaController.$inject = ['$scope', '$produtosCaracteristica', '$mdDialog', '$window'];
    function ListaProdutosCaracteristicaController($scope, $produtosCaracteristica, $mdDialog, $window) {
        const limit = 10;

        $scope.listaCaracteristicas = [];
        $scope.page = 1;
        $scope.setPage = setPage;
        $scope.isPage = isPage;
        $scope.totalRegistros = 0;
        $scope.numberOfPages = 1;
        $scope.pages = [];
        $scope.confirmarExclusao = confirmarExclusao;

        function loadLista() {
            $produtosCaracteristica
                .getListagem($scope.page, limit)
                .then((response) => {
                    $scope.listaCaracteristicas = response.data.data;
                    $scope.totalRegistros = response.data.totalRows;
                    $scope.numberOfPages = response.data.numberOfPages;
                    loadPages($scope.numberOfPages);
                    calculateRanges();
                })
        }

        function confirmarExclusao(noticia, ev) {

            const confirm = $mdDialog.confirm()
                .title(noticia.titulo)
                .textContent('Deseja realmente excluir?')
                .ariaLabel('Exclusão características')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');

            const apagando = $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Aguarde')
                .textContent('Apagando características')
                .ariaLabel('Apagando')
                .targetEvent(ev);

            const show = $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Sucesso')
                .textContent('Apagado com sucesso')
                .ariaLabel('Apagado com Sucesso')
                .ok('OK')
                .targetEvent(ev);

            $mdDialog.show(confirm).then(function () {

                $mdDialog.show(apagando);
                $produtosCaracteristica
                    .excluir(noticia.id)
                    .then((response) => {
                        $mdDialog.hide(apagando);
                        $mdDialog.show(show).then(function () {
                            $window.location.reload();
                        });
                    });
            }, () => {
            });
        }

        function loadPages(total) {
            $scope.pages = [];
            for (let i = 0; i < total; i++)
                $scope.pages.push(parseInt(i) + 1);
        }

        function calculateRanges() {
            $scope.rangeMin = (($scope.page - 1) * limit) + 1;
            $scope.rangeMax = $scope.page * limit;
            if ($scope.rangeMax > $scope.totalRegistros)
                $scope.rangeMax = $scope.totalRegistros;
        }

        function setPage(page) {
            $scope.page = page;
            loadLista();
            calculateRanges();
        }

        function isPage(page) {
            return $scope.page === page;
        }

        (function () {
            loadLista();
        })();
    }

})();