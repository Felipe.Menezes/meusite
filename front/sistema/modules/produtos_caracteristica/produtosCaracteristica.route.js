(function () {

    'use strict';

    angular
        .module('produtosCaracteristica')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/produtos-caracteristica", {
                templateUrl: 'modules/produtos_caracteristica/lista/lista.html',
                controller: 'ListaProdutosCaracteristicaController'
            })
            .when("/produtos-caracteristica/cadastrar", {
                templateUrl: 'modules/produtos_caracteristica/cadastro/cadastro.html',
                controller: 'CadastroProdutosCaracteristicaController'
            })
            .when("/produtos-caracteristica/editar/:id", {
                templateUrl: 'modules/produtos_caracteristica/cadastro/cadastro.html',
                controller: 'EditarProdutosCaracteristicaController'
            });
    }

})();