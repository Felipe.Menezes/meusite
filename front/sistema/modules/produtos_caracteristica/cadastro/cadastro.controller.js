(function () {

    'use strict';

    angular
        .module('produtosCaracteristica')
        .controller('CadastroProdutosCaracteristicaController', CadastroProdutosCaracteristicaController);

    CadastroProdutosCaracteristicaController.$inject = ['$scope', '$produtosCaracteristica', '$location', '$timeout'];
    function CadastroProdutosCaracteristicaController($scope, $produtosCaracteristica, $location, $timeout) {
        $scope.titulo = "Nova Característica";
        $scope.categoria = {};
        $scope.salvar = salvar;
        const salvarButton = $("#salvar");

        function salvar(caracteristica) {
            if (!categoriaValida(caracteristica))
                return;

            salvarButton.text("Salvando...");
            salvarButton.attr("disabled", "disabled");

            $produtosCaracteristica
                .cadastrar(caracteristica)
                .then((response) => {
                    salvarButton.removeClass("btn-primary");
                    salvarButton.addClass("btn-success");
                    salvarButton.removeAttr("disabled");
                    salvarButton.text("Salvo");
                    $timeout(function () {
                        $location.path("/produtos-caracteristica");
                    }, 1000);

                })
                .catch((response) => {
                    alert("Erro ao salvar!");
                    console.error(response);
                });
        }

        function categoriaValida(caracteristica) {
            if (!caracteristica.descricao) {
                alert("Defina uma descrição para a característica");
                return false;
            }
            return true;
        }

    }

})();