(function () {

    'use strict';

    angular
        .module('produtosCaracteristica')
        .controller('EditarProdutosCaracteristicaController', EditarProdutosCaracteristicaController);

    EditarProdutosCaracteristicaController.$inject = ['$scope', '$routeParams', '$produtosCaracteristica', '$location', '$timeout'];
    function EditarProdutosCaracteristicaController($scope, $routeParams, $produtosCaracteristica, $location, $timeout) {
        $scope.titulo = "Alterar Característica #" + $routeParams.id;
        $scope.caracteristica = {};
        $scope.salvar = salvar;
        const salvarButton = $("#salvar");

        function salvar(caracteristica) {
            if (!produtoValida(caracteristica))
                return;

            salvarButton.text("Salvando...");
            salvarButton.attr("disabled", "disabled");

            $produtosCaracteristica
                .putCaracteristica($routeParams.id, caracteristica)
                .then(function (response) {
                    salvarButton.removeClass("btn-primary");
                    salvarButton.addClass("btn-success");
                    salvarButton.removeAttr("disabled");
                    salvarButton.text("Salvo");
                    $timeout(function () {
                        salvarButton.removeClass("btn-success");
                        salvarButton.addClass("btn-primary");
                        salvarButton.text("Salvar");
                        $location.path("/produtos-caracteristica");
                    }, 1000);
                })
                .catch(function (response) {
                    console.error(response.data);

                    salvarButton.removeClass("btn-primary");
                    salvarButton.addClass("btn-danger");
                    salvarButton.removeAttr("disabled");
                    salvarButton.text("Erro");
                    setTimeout(function () {
                        salvarButton.removeClass("btn-success");
                        salvarButton.addClass("btn-primary");
                        salvarButton.text("Salvar");
                    }, 3000);
                });

        }

        function load() {
            $produtosCaracteristica
                .getCaracteristica($routeParams.id)
                .then(function (response) {
                    $scope.caracteristica  = response.data;
                })
                .catch(function (response) {
                    alert("ERRO");
                    console.error(response);
                });
        }

        function produtoValida(caracteristica) {
            if (!caracteristica.descricao) {
                alert("Defina uma descrição para a característica");
                return false;
            }
            return true;
        }

        (function () {
            load();
        })();

    }

})();