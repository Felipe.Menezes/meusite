(function () {

    'use strict';

    angular
        .module('sistema')
        .directive('ngFileModel', FileModelDirective);

    FileModelDirective.$inject = ['$parse'];
    function FileModelDirective($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                const model = $parse(attrs.ngFileModel);
                const isMultiple = attrs.multiple;
                const modelSetter = model.assign;
                element.bind('change', function () {
                    const values = [];
                    angular.forEach(element[0].files, function (item) {
                        values.push(item);
                    });

                    scope.$apply(function () {
                        if (isMultiple) {
                            modelSetter(scope, values);
                        } else {
                            modelSetter(scope, values[0]);
                        }
                    });
                });
            }
        };
    }

})();