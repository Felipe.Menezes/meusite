(function () {

    'use strict';

    angular
        .module("produtos_categoria")
        .factory('$produtosCategoria', ProdutosService);

    ProdutosService.$inject = ['$http', '$rootScope'];
    function ProdutosService($http, $rootScope) {
        const produtosCategoriaAPI = $rootScope.urlApi + "ProdutosCategoria.php";

        function getListagem(page, limit) {
            return $http.get(produtosCategoriaAPI + "?page=" + page + "&limit=" + limit);
        }

        function getAll() {
            return $http.get(produtosCategoriaAPI + "/WithoutPagination");
        }

        function cadastrar(categoria) {
            return $http.post(produtosCategoriaAPI, getDataCadastrar(categoria));
        }

        function getDataCadastrar(categoria) {
            return {
                descricao: categoria.descricao
            };
        }

        function getCategoria(id) {
            return $http.get(produtosCategoriaAPI + "/" + id);
        }

        function putCategoria(id, categoria) {
            const data = {
                descricao: categoria.descricao
            };

            return $http.put(produtosCategoriaAPI + "/" + id, data);
        }

        function excluir(id) {
            return $http.delete(produtosCategoriaAPI + "/" + id);
        }

        return {
            getListagem: getListagem,
            getAll: getAll,
            cadastrar: cadastrar,
            getCategoria: getCategoria,
            putCategoria: putCategoria,
            excluir: excluir
        };
    }

})();