(function () {

    'use strict';

    angular
        .module('sistema')
        .factory('$geo', GeoService);

    GeoService.$inject = ['$http', '$rootScope'];
    function GeoService($http, $rootScope) {
        function getEstados() {
            return $http
                .get($rootScope.urlApi + "Estados.php");
        }

        function getCidades(idEstado) {
            return $http
                .get($rootScope.urlApi + "Cidades.php/porEstado/" + idEstado.toString());
        }

        return {
            getEstados: getEstados,
            getCidades: getCidades
        }
    }
})();