(function () {

    'use strict';

    angular
        .module('assistenciasDistribuidores')
        .factory('$assistenciasDistribuidores', AssistenciasDistribuidores);

    AssistenciasDistribuidores.$inject = ['$rootScope', '$http'];
    function AssistenciasDistribuidores($rootScope, $http) {
        const urlApi = $rootScope.urlApi + "AssistenciasDistribuidores.php";

        function cadastrar(modelo) {
            return $http
                .post(urlApi, modelo);
        }

        function put(id, modelo) {
            return $http
                .put(urlApi + "/" + id, modelo);
        }

        function getAll(page, limit) {
            return $http
                .get(mountUrlGetAll(page, limit));
        }

        function getAllDistribuidores(page, limit) {
            return $http
                .get(mountUrlGetAllDistribuidores(page, limit));
        }

        function getAllAssistencias(page, limit) {
            return $http
                .get(mountUrlGetAllAssistencias(page, limit));
        }

        function mountUrlGetAll(page, limit) {
            return urlApi + "?page=" + page + "&limit=" + limit;
        }

        function mountUrlGetAllDistribuidores(page, limit) {
            return urlApi + "/Distribuidores/?page=" + page + "&limit=" + limit;
        }

        function mountUrlGetAllAssistencias(page, limit) {
            return urlApi + "/Assistencias/?page=" + page + "&limit=" + limit;
        }

        return {
            cadastrar: cadastrar,
            put: put,
            getAll: getAll,
            getAllDistribuidores: getAllDistribuidores,
            getAllAssistencias: getAllAssistencias
        };
    }

})();