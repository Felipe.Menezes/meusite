(function () {

    'use strict';

    angular
        .module('usuarios', [])
        .factory('$user', UserFactory);

    UserFactory.$inject = ['$rootScope', '$http', '$autenticacao'];
    function UserFactory($rootScope, $http, $autenticacao) {

        function get(idUsuario, callback) {
            var urlApi = $rootScope.urlApi + "Usuario.php";
            $http
                .get(urlApi + "/" + idUsuario)
                .then(function (response) {
                    if (response.status === 200)
                        callback(response.data);
                });
        }

        return {
            get: get
        }
    }

})();