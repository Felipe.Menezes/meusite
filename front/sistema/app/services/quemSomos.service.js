(function () {

    'use strict';

    angular
        .module('sistema')
        .factory("$quemSomos", ConfigSite);

    ConfigSite.$inject = ['$rootScope', '$http'];
    function ConfigSite($rootScope, $http) {
        function get(callback) {
            const urlApi = $rootScope.urlApi + "QuemSomos.php";
            return $http
                .get(urlApi);
        }

        function put(data, callback) {
            const urlApi = $rootScope.urlApi + "QuemSomos.php";

            return $http
                .put(urlApi, data);
        }

        return {
            get: get,
            put: put
        }
    }

})();