(function () {

    'use strict';

    angular
        .module('login')
        .factory("$autenticacao", AuthenticationService)
        .factory("$login", Login);

    Login.$inject = ['$http', '$cookies'];
    function Login($http, $cookies) {
        function logar(login, senha, error, ) {

            var post = $http.post('http://api.evoxx.com.br/sistema/controllers/Autenticacao.php/logar', {
                login: login,
                senha: senha
            });

            post.then(function (response) {
                if (response.status === 200 && response.data.token) {
                    $cookies.put("token", response.data.token, {
                        path: '/'
                    });
                    window.location = "../";
                }
                return false;
            });

            post.catch(function (response) {
                error(response.data);
            });
        }

        return {
            logar: logar
        };
    }

    AuthenticationService.$inject = ['$cookies'];
    function AuthenticationService($cookies) {

        function getToken() {
            return $cookies.get("token");
        }

        function tokenValido() {
            if (!$cookies.get("token"))
                return false;

            var token = getToken();
            var jsonData = JSON.parse(decodeBase64(token.split(".")[1]));

            var dataToken = new Date(jsonData.data);
            var tomorrow = new Date().setDate(new Date().getDate() + 1);

            return dataToken < tomorrow;
        }

        function getIdUsuarioLogado() {
            var tokenId = getToken().split(".")[1];
            var decoded = decodeBase64(tokenId);

            return JSON.parse(decoded).idUsuario;
        }

        function deslogar() {
            resetCredentials();
            window.location = "login/";
        }

        function resetCredentials() {
            $cookies.remove("token", {
                path: '/'
            });
        }

        return {
            resetCredentials: resetCredentials,
            getToken: getToken,
            tokenValido: tokenValido,
            getIdUsuarioLogado: getIdUsuarioLogado,
            deslogar: deslogar
        };
    }

    function decodeBase64(s) {
        var e = {}, i, b = 0, c, x, l = 0, a, r = '', w = String.fromCharCode, L = s.length;
        var A = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        for (i = 0; i < 64; i++) {
            e[A.charAt(i)] = i;
        }
        for (x = 0; x < L; x++) {
            c = e[s.charAt(x)];
            b = (b << 6) + c;
            l += 6;
            while (l >= 8) {
                ((a = (b >>> (l -= 8)) & 0xff) || (x < (L - 2))) && (r += w(a));
            }
        }
        return r;
    }

})();