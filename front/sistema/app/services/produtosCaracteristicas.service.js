(function () {

    'use strict';

    angular
        .module("produtosCaracteristica")
        .factory('$produtosCaracteristica', ProdutosCaracteristicaService);

    ProdutosCaracteristicaService.$inject = ['$http', '$rootScope'];
    function ProdutosCaracteristicaService($http, $rootScope) {
        const produtosCategoriaAPI = $rootScope.urlApi + "ProdutosCaracteristica.php";

        function getListagem(page, limit) {
            return $http.get(produtosCategoriaAPI + "?page=" + page + "&limit=" + limit);
        }

        function getAll() {
            return $http.get(produtosCategoriaAPI + "/WithoutPagination");
        }

        function cadastrar(categoria) {
            return $http.post(produtosCategoriaAPI, categoria);
        }

        function getCaracteristica(id) {
            return $http.get(produtosCategoriaAPI + "/" + id);
        }

        function putCaracteristica(id, categoria) {
            const data = {
                descricao: categoria.descricao
            };

            return $http.put(produtosCategoriaAPI + "/" + id, data);
        }

        function excluir(id) {
            return $http.delete(produtosCategoriaAPI + "/" + id);
        }

        return {
            getListagem: getListagem,
            getAll: getAll,
            cadastrar: cadastrar,
            getCaracteristica: getCaracteristica,
            putCaracteristica: putCaracteristica,
            excluir: excluir
        };
    }

})();