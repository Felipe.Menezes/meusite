(function () {

    'use strict';

    angular
        .module("sistema")
        .factory("APIInterceptor", APIInterceptor);

    APIInterceptor.$inject = ['$autenticacao'];
    function APIInterceptor($autenticacao) {
        function request(config) {
            var accessToken = $autenticacao.getToken() || 'unauthorized';
            config.headers.Token = accessToken;
            return config;
        }

        return {
            request: request
        };
    }

})();