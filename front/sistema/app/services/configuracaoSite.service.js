(function () {

    'use strict';

    angular
        .module('sistema')
        .factory("$configuracaoSite", ConfigSite);

    ConfigSite.$inject = ['$rootScope', '$autenticacao', '$http'];
    function ConfigSite($rootScope, $autenticacao, $http) {
        function get(callback) {
            var urlApi = $rootScope.urlApi + "ConfiguracaoSite.php";
            $http
                .get(urlApi)
                .then(function (response) {
                    if (response.status === 200)
                        callback(response.data);
                });
        }

        function put(configuracao, callback) {
            var urlApi = $rootScope.urlApi + "ConfiguracaoSite.php";
            
            var data = {
                conheca_nossos_produtos: configuracao.conhecaNossosProdutos
            };

            $http
                .put(urlApi, data)
                .then(function (response) {
                    if (response.status === 200)
                        callback(response.data);
                });
        }

        return {
            get: get,
            put: put
        }
    }

})();