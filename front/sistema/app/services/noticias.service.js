(function () {

    'use strict';

    angular
        .module("noticias")
        .factory('$noticias', NoticiasService);

    var noticiasListagem = [
        {id: 1, data: '22/11/1996 15:45', titulo: 'Teste de título 1'},
        {id: 2, data: '22/11/2017 15:45', titulo: 'Teste de título 2'},
        {id: 3, data: '22/05/1996 15:45', titulo: 'Teste de título 3'},
        {id: 4, data: '01/11/1996 15:45', titulo: 'Teste de título 4'},
    ];

    NoticiasService.$inject = ['$http', '$rootScope', '$autenticacao'];
    function NoticiasService($http, $rootScope, $autenticacao) {
        var noticiaApi = $rootScope.urlApi + "Noticia.php";

        function getListagem(options) {
            options = options || (options = {});

            var defaults = {
                page: 1,
                limit: 10,
                then: function () {
                },
                catch: function () {
                }
            }

            var innerOpt = $.extend(defaults, options);

            var url = noticiaApi + "?page=" + innerOpt.page + "&limit=" + innerOpt.limit;

            $http
                .get(url)
                .then(function (response) {
                    if (response.status === 200)
                        innerOpt.then(response.data);
                })
                .catch(function (response) {
                    innerOpt.catch(response.data);
                });

        }

        function cadastrar(noticia, imagens) {

            const modelo = {
                id_autor: $autenticacao.getIdUsuarioLogado(),
                titulo: noticia.titulo,
                artigo: noticia.artigo,
                data_publicacao: noticia.data_publicacao,
            };

            const data = {
                model: modelo,
                files: imagens
            };

            function myTransform(data) {
                const formData = new FormData();

                for (const key in data.model) {
                    formData.append(key, data.model[key]);
                }

                if (data.files) {
                    for (let i = 0; i < data.files.length; i++) {
                        formData.append("imagens[]", data.files[i]);
                    }
                }
                return formData;
            }

            return $http
                .post(noticiaApi, data, {
                    transformRequest: myTransform,
                    headers: {'Content-Type': undefined}
                })
        }

        function getNoticia(id, callback, error) {
            var url = noticiaApi + "/" + id;

            $http
                .get(url)
                .then(function (response) {
                    if (response.status === 200)
                        callback(response.data);
                })
                .catch(function (response) {
                    error(response.data);
                });
        }

        function putNoticia(id, noticia) {
            var url = noticiaApi + "/" + id;

            var data = noticia;
            data.id_autor = $autenticacao.getIdUsuarioLogado();

            return $http.put(url, data);
        }

        function excluir(id) {
            var url = noticiaApi + "/" + id;

            return $http
                .delete(url);
        }

        function removerFoto(idFoto) {
            return $http
                .delete(noticiaApi + "/" + idFoto + "/RemoverFoto");
        }

        function adicionarFotos(idNoticia, fotos) {
            const url = noticiaApi + "/" + idNoticia + "/AdicionarFoto";
            const data = new FormData();

            fotos
                .forEach((i) =>
                    data.append("imagens[]", i)
                );

            return $http.post(url, data, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            });
        }

        return {
            getListagem: getListagem,
            cadastrar: cadastrar,
            getNoticia: getNoticia,
            putNoticia: putNoticia,
            adicionarFotos: adicionarFotos,
            removerFoto: removerFoto,
            excluir: excluir
        };
    }

})();