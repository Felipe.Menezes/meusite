(function () {

    'use strict';

    angular
        .module("produtos")
        .factory('$produtos', ProdutosService);

    ProdutosService.$inject = ['$http', '$rootScope'];
    function ProdutosService($http, $rootScope) {
        const produtoApi = $rootScope.urlApi + "Produtos.php";

        function getListagem(page, limit) {
            return $http.get(produtoApi + "?page=" + page + "&limit=" + limit);
        }

        function cadastrar(produto, fotos, manual) {
            const data = {
                model: produto,
                fotos: fotos,
                manual: manual
            };

            function myTransform(data) {
                const formData = new FormData();

                for (const key in data.model) {
                    formData.append(key, data.model[key]);
                }

                if (data.manual)
                    formData.append("manual", data.manual);

                if (data.fotos) {
                    for (let i = 0; i < data.fotos.length; i++) {
                        formData.append("imagens[]", data.fotos[i]);
                    }
                }
                return formData;
            }

            return $http.post(produtoApi, data, {
                transformRequest: myTransform,
                headers: {'Content-Type': undefined}
            });
        }

        function getProduto(id) {
            return $http.get(getUrlWIthId(id));
        }

        function putProduto(id, produto) {
            return $http.put(getUrlWIthId(id), produto);
        }

        function excluir(id) {
            return $http.delete(getUrlWIthId(id));
        }

        function salvarCaracteristica(idProduto, caracteristica) {
            return $http.post(getUrlWIthId(idProduto) + "/AdicionarCaracteristica", caracteristica);
        }

        function excluirCaracteristica(idProduto, caracteristica) {
            return $http.delete(getUrlWIthId(idProduto) + "/ExcluirCaracteristica/" + caracteristica.id);
        }

        function salvarEspecificacao(idProduto, especificacao) {
            return $http.post(getUrlWIthId(idProduto) + "/AdicionarEspecificacao", especificacao);
        }

        function excluirEspecificacao(idProduto, especificacao) {
            return $http.delete(getUrlWIthId(idProduto) + "/ExcluirEspecificacao/" + especificacao.id);
        }

        function getUrlWIthId(idProduto) {
            return produtoApi + "/" + idProduto;
        }

        function getCaracteristicas(idProduto) {
            return $http.get(getUrlWIthId(idProduto) + "/Caracteristicas");
        }

        function getEspecificacoes(idProduto) {
            return $http.get(getUrlWIthId(idProduto) + "/Especificacoes");
        }

        function adicionarFotos(idProduto, fotos) {
            const url = getUrlWIthId(idProduto) + "/AdicionarFoto";
            const data = new FormData();

            fotos
                .forEach((i) =>
                    data.append("imagens[]", i)
                );

            return $http.post(url, data, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            });
        }

        function setManual(idProduto, manual) {
            const url = getUrlWIthId(idProduto) + "/SetManual";
            const data = new FormData();

            data.append("manual", manual);

            return $http.post(url, data, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            });
        }

        function removerFoto(idFoto) {
            return $http
                .delete(getUrlWIthId(idFoto) + "/RemoverFoto");
        }

        return {
            adicionarFotos: adicionarFotos,
            removerFoto: removerFoto,
            getListagem: getListagem,
            cadastrar: cadastrar,
            getProduto: getProduto,
            getCaracteristicas: getCaracteristicas,
            getEspecificacoes: getEspecificacoes,
            putProduto: putProduto,
            salvarCaracteristica: salvarCaracteristica,
            excluirCaracteristica: excluirCaracteristica,
            salvarEspecificacao: salvarEspecificacao,
            excluirEspecificacao: excluirEspecificacao,
            setManual: setManual,
            excluir: excluir
        };
    }

})();