(function () {

    'use strict';

    const dependencies = [
        'home',
        'login',
        'usuarios',
        'noticias',
        'produtos',
        'produtos_categoria',
        'produtosCaracteristica',
        'configuracaoSite',
        'quemSomos',
        'assistenciasDistribuidores',
        'ngRoute',
        'ngCookies',
        'ngMaterial'
    ];

    angular
        .module('sistema', dependencies)
        .config(configApp)
        .run(run);

    configApp.$inject = ['$routeProvider', '$httpProvider'];
    function configApp($routeProvider, $httpProvider) {
        $routeProvider
            .otherwise({ redirectTo: '/home' });

        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.cookie = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};

        $httpProvider.defaults.headers.common['Content-type'] = 'application/json; charset=utf-8;';
        $httpProvider.interceptors.push('APIInterceptor');
    }

    run.$inject = ['$rootScope', '$autenticacao', '$user', '$http', '$location'];
    function run($rootScope, $autenticacao, $user, $http, $location) {
        $rootScope.urlApi = "http://api.evoxx.com.br/sistema/controllers/";
        //$rootScope.urlApi = "http://localhost/Evoxx/back/sistema/controllers/";
        $rootScope.goToCadastrar = goToCadastrar;
        $rootScope.goToEditar = goToEditar;

        $rootScope.$on('$locationChangeStart', function (event, next, current) {

            if (!$autenticacao.tokenValido()) {
                event.preventDefault();
                window.location = "login/";
            }

            $user.get($autenticacao.getIdUsuarioLogado(), function (response) {
                $rootScope.usuarioLogado = response
            });
        });

        $rootScope.$on("$locationChangeSuccess", function (event, toState, toParams, fromState, fromParams) {

        });

        function goToCadastrar() {
            var path = $location.path();
            $location.path($location.path() + "/cadastrar");
        }

        function goToEditar(id) {
            $location.path($location.path() + "/editar/" + id);
        }

    }

    $.fn.hide = function () {
        $(this).css("display", "none");
    }
})();