(function () {

    'use strict';

    angular
        .module('sistema')
        .controller("MainController", MainController);

    MainController.$inject = ['$scope', '$autenticacao', '$location', '$rootScope'];

    function MainController($scope, $autenticacao, $location, $rootScope) {
        $scope.sair = sair;
        $scope.goTo = goTo;
        
        function goTo(path){
            $location.path(path);
        }

        function sair() {
            $autenticacao.deslogar();
        }
    }

})();