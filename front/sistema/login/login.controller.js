(function () {

    'use strict';

    angular
        .module('login')
        .controller("LoginController", LoginController);

    LoginController.$inject = ['$scope', '$autenticacao', '$login', '$location'];
    function LoginController($scope, $autenticacao, $login, $location) {
        $scope.usuario = "";
        $scope.senha = "";
        $scope.logar = logar;
        $scope.dataLoading = false;

        (function () {
            $autenticacao.resetCredentials();
        })();

        function logar(login, senha) {
            $scope.dataLoading = true;
            $login.logar(login, senha, function (response) {
                $scope.dataLoading = false;
                alert(response.response);
            });
        }
    }

})();
