(function () {

    'use strict';

    angular
        .module("evoxx")
        .config(ConfigurationRoutes);

    
    function ConfigurationRoutes($routeProvider) {

        $routeProvider
            .when('/', {
                templateUrl: 'index/index.html',
                controller: 'IndexController'
            });
    }

})();