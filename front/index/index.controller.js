(function () {

    'use strict';

    angular
        .module("evoxx")
        .controller("IndexController", IndexController);

    IndexController.$inject = ['$scope', '$configuracaoSite', '$noticias', '$produtos', '$sce'];

    function initializeCarousel() {
        $('.owl-carousel').owlCarousel({
            items:3,
            lazyLoad:true,
            loop:false,
            margin:10,
            autoplay:true,
            autoplayTimeout:4000,
            autoplayHoverPause:true
        });
    }

    function IndexController($scope, $configuracaoSite, $noticias, $produtos, $sce) {
        $scope.conhecaNossosProdutos = "";
        $scope.noticias = [];
        $scope.produtos = [];

        $configuracaoSite.get(response => $scope.conhecaNossosProdutos = response.conheca_nossos_produtos);
        $produtos.getAll().then(response => {
            $scope.produtos = response.data;
            angular.element(initializeCarousel);
        });
        $noticias.getForIndex().then(res => $scope.noticias = res.data);

        init();
    }



    function init() {

        $("#home-logo").height($(window).height());
        $("[data-bg]").each(function (idx, item) {

            var $this = $(item);
            var dataBg = $this.data("bg");

            $this.css("background-image", "url(" + dataBg + ")");
            $this.removeAttr("data-bg");
        });

        function nextSlide() {

            var slides = $(".child-slide-logo");
            var active = -1;
            slides.each(function (idx) {
                if ($(this).hasClass("active"))
                    active = idx
            });

            if (active > -1) {
                slides.eq(active).hide();
                slides.eq(active).removeClass("active");
            }
            if (active === (slides.length - 1))
                active = -1;

            slides.eq(active + 1).fadeIn(200);
            slides.eq(active + 1).addClass("active");

            setTimeout(nextSlide, 10000);
        }

        nextSlide();
    }

})();