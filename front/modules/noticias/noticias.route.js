(function () {

    'use strict';

    angular
        .module('noticias')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/noticias", {
                templateUrl: "modules/noticias/lista/lista.html",
                controller: "ListaNoticiasController"
            });

        $routeProvider
            .when("/noticia/:id", {
                templateUrl: 'modules/noticias/visualiza/visualiza.html',
                controller: "VisualizaNoticiaController"
            })
    }
})();