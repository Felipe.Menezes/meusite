(function () {

    'use strict';

    angular
        .module('noticias')
        .controller('ListaNoticiasController', ListaNoticiasController);

    ListaNoticiasController.$inject = ['$scope', '$noticias', '$sce'];
    function ListaNoticiasController($scope, $noticias, $sce) {
        const limit = 4;
        $scope.carregandoDados = true;
        $scope.noticias = [];
        $scope.page = 1;
        $scope.setPage = setPage;
        $scope.isPage = isPage;
        $scope.totalRegistros = 0;
        $scope.numberOfPages = 1;
        $scope.pages = [];
        $scope.rangeMax = 0;
        $scope.rangeMin = 0;

        function loadLista() {
            $noticias
                .getAll($scope.page, limit)
                .then(res => {
                    $scope.noticias = res.data.data;
                    $scope.carregandoDados = false;
                    $scope.numberOfPages = res.data.numberOfPages;
                    $scope.totalRegistros = res.data.totalRows;
                    loadPages($scope.numberOfPages);
                    calculateRanges();
                });
        }

        function loadPages(total){
            $scope.pages = [];

            for (let i = 0; i < total; i++)
                $scope.pages.push(parseInt(i) + 1);
        }

        function calculateRanges() {
            $scope.rangeMin = (($scope.page - 1) * limit) + 1;
            $scope.rangeMax = $scope.page * limit;
            if ($scope.rangeMax > $scope.totalRegistros)
                $scope.rangeMax = $scope.totalRegistros;
        }

        function setPage(page) {
            $scope.page = page;
            loadLista();
            calculateRanges();
        }

        function isPage(page) {
            return $scope.page === page;
        }

        loadLista();
        initCarousel();
    }

    function initCarousel() {
        $(document).ready(() => {
            $(".noticia-fotos").owlCarousel();
        });
    }

})();