(function () {

    'use strict';

    angular
        .module('noticias')
        .controller("VisualizaNoticiaController", VisualizaNoticiaController);

    function initializeCarousel() {
        $('.owl-carousel').owlCarousel({
            items: 1,
            lazyLoad: true,
            loop: false,
            margin: 10
        });
    }

    VisualizaNoticiaController.$inject = ['$scope', '$noticias', '$routeParams', '$sce'];
    function VisualizaNoticiaController($scope, $noticias, $routeParams, $sce) {
        $scope.noticia = {};

        $scope.carregandoDados = true;

        $noticias.get($routeParams.id).then(res => {
            $scope.carregandoDados = false;
            $scope.noticia = res.data;
            angular.element(initializeCarousel);
        });
    }
})();