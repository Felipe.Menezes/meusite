(function () {

    'use strict';

    angular
        .module('acessorestrito')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/acessorestrito", {
                templateUrl: "modules/acessoRestrito/acessoRestrito.html",
                controller: "AcessoRestritoController"
            });
    }
})();