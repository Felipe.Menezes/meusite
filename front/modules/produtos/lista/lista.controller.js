(function () {

    'use strict';

    angular
        .module('produtos')
        .controller('ListaProdutosController', ListaProdutosController);

    ListaProdutosController.$inject = ['$scope', '$routeParams', '$produtos', '$produtoCategorias'];
    function ListaProdutosController($scope, $routeParams, $produtos, $produtoCategorias) {
        var produtosFinalizado = false, 
            categoriaFinalizado = false;
        
        $scope.id_categoria = $routeParams.id_categoria;
        $scope.carregandoDados = true;
        $scope.produtos = [];

        $produtos
            .getByCategoria($scope.id_categoria)
            .then(res => {
                $scope.produtos = res.data
                produtosFinalizado = true;
                setFinalizado();
            })
            .catch(res => {});

        $produtoCategorias
            .getById($scope.id_categoria)
            .then(res => {
                $scope.categoria = res.data[0];
                categoriaFinalizado = true;
                setFinalizado();
            })

        function setFinalizado(){
            $scope.carregandoDados = !(produtosFinalizado && categoriaFinalizado);
        }
    }

})();