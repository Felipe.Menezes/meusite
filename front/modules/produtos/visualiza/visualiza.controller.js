(function () {

    'use strict';

    angular
        .module('produtos')
        .controller('VisualizaProdutoController', VisualizaProdutoController);

    function initializeCarousel() {

        $('.owl-carousel').owlCarousel({
            items: 1,
            lazyLoad: true,
            loop: false,
            margin: 10
        });
    }

    VisualizaProdutoController.$inject = ['$scope', '$routeParams', '$produtos'];
    function VisualizaProdutoController($scope, $routeParams, $produtos) {
        $scope.categoria = {};
        let tabSelected = 1;

        $produtos.getById($routeParams.id_produto).then(res => {
            $scope.categoria = res.data;
            angular.element(initializeCarousel);
        });

        $scope.isTab = (tab) => tabSelected === tab;
        $scope.setTab = (tab) => tabSelected = tab;

        $produtos
            .getCaracteristicas($routeParams.id_produto)
            .then((res) => $scope.caracteristicas = res.data);

        $produtos
            .getEspecificacoes($routeParams.id_produto)
            .then((res) => $scope.especificacoes = res.data);
    }

    var testeEspec = [
        {
            titulo: 'Dimensões',
            valor: 'Largura: 16,6cm Altura: 18,0cm Comprimento: 19,8cm'
        },
        {
            titulo: 'Peso Líquido',
            valor: '2,18 Kg'
        },
    ];

    var testeCarac = [
        {
            descricao: "Descrição",
            items: [
                {
                    caracteristica: "1"
                },
                {
                    caracteristica: "Carac 2"
                },
                {
                    caracteristica: "outro carac"
                },
            ]
        },
        {
            descricao: "Descrição",
            items: [
                {
                    caracteristica: "1"
                },
                {
                    caracteristica: "Carac 2"
                },
                {
                    caracteristica: "outro carac"
                },
            ]
        }
    ];

})();