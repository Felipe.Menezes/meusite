(function () {

    'use strict';

    angular
        .module('produtos')
        .config(RouteConfig);

    RouteConfig.$inject = ['$routeProvider'];
    function RouteConfig($routeProvider) {
        $routeProvider.when('/produto/:id_produto', {
            templateUrl: 'modules/produtos/visualiza/visualiza.html',
            controller: 'VisualizaProdutoController'
        });

        $routeProvider.when('/produtos/:id_categoria', {
            templateUrl: 'modules/produtos/lista/lista.html',
            controller: 'ListaProdutosController'
        });
    }

})();