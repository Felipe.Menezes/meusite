(function () {

    'use strict';

    angular
        .module('contato')
        .controller("ContatoController", ContatoController);

    ContatoController.$inject = ['$scope'];
    function ContatoController($scope) {
        init();
    }

    function init() {
        $('.maps').click(function () {
            $('.maps iframe').css("pointer-events", "auto");
        });

        $(".maps").mouseleave(function () {
            $('.maps iframe').css("pointer-events", "none");
        });
    }
})();