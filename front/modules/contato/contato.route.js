(function () {

    'use strict';

    angular
        .module('contato')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/contato", {
                templateUrl: "modules/contato/contato.html",
                controller: "ContatoController"
            });
    }
})();