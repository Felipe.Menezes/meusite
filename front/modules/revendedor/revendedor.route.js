(function () {

    'use strict';

    angular
        .module('revendedor')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/revendedor", {
                templateUrl: "modules/revendedor/revendedor.html",
                controller: "RevendedorController"
            });
    }
})();