(function () {

    'use strict';

    angular
        .module('quemSomos')
        .controller("QuemSomosController", QuemSomosController);

    QuemSomosController.$inject = ['$scope', '$quemSomos', '$sce'];
    function QuemSomosController($scope, $quemSomos,$sce) {
        $scope.titulo = "";
        $scope.artigo = "Carregando, aguarde...";

        $quemSomos.get().then(res => {
            $scope.titulo = res.data.titulo;
            $scope.artigo = res.data.artigo;
        }).catch(res => {
            $scope.artigo = "Falha ao carregar, tente novamente!"
            console.error(res);
        });
    }
})();