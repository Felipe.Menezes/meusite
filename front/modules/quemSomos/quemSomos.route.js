(function () {

    'use strict';

    angular
        .module('quemSomos')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/quemSomos", {
                templateUrl: "modules/quemSomos/quemSomos.html",
                controller: "QuemSomosController"
            });
    }
})();