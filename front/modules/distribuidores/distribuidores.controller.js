(function () {

    'use strict';

    angular
        .module('distribuidores')
        .controller("DistribuidoresController", DistribuidoresController);

    DistribuidoresController.$inject = ['$scope', '$geo', '$distribuidores'];
    function DistribuidoresController($scope, $geo, $distribuidores) {
        $scope.estados = [];
        $scope.cidades = [];
        $scope.carregarCidades = carregarCidades;
        $scope.buscarCompleto = buscarCompleto;
        $scope.distribuidores = [];
        $scope.buscando = false;

        $geo.getEstados().then(res => $scope.estados = res.data);

        function carregarCidades(idEstado) {
            $geo.getCidades(idEstado).then(res => $scope.cidades = res.data);
            buscarByEstado(idEstado);
        }

        function buscarByEstado(idEstado){
            $scope.buscando = true;
            $distribuidores.getByEstado(idEstado)
                .then(res => {
                    $scope.distribuidores = res.data;
                    $scope.buscando = false;
                });
        }

        function buscarCompleto(busca){
            $scope.buscando = true;
            $distribuidores.getByCidade(busca.estado.id, busca.cidade.id)
                .then(res => {
                    $scope.distribuidores = res.data
                    $scope.buscando = false;
                });
        }
    }
})();