(function () {

    'use strict';

    angular
        .module('distribuidores')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/distribuidores", {
                templateUrl: "modules/distribuidores/distribuidores.html",
                controller: "DistribuidoresController"
            });
    }
})();