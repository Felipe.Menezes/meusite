(function () {

    'use strict';

    angular
        .module('sejaassistencia')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/sejaassistencia", {
                templateUrl: "modules/seja-assistencia/seja-assistencia.html",
                controller: "sejaAssistenciaController"
            });
    }
})();