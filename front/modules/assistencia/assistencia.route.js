(function () {

    'use strict';

    angular
        .module('assistencia')
        .config(ConfigRoute);

    ConfigRoute.$inject = ['$routeProvider'];
    function ConfigRoute($routeProvider) {
        $routeProvider
            .when("/assistencia", {
                templateUrl: "modules/assistencia/assistencia.html",
                controller: "AssistenciaController"
            });
    }
})();