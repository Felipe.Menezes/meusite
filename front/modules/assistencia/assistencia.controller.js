(function () {

    'use strict';

    angular
        .module('assistencia')
        .controller("AssistenciaController", AssistenciaController);

    AssistenciaController.$inject = ['$scope', '$geo', '$assistenciaTecnica'];
    function AssistenciaController($scope, $geo, $assistenciaTecnica) {
        $scope.estados = [];
        $scope.cidades = [];
        $scope.carregarCidades = carregarCidades;
        $scope.buscarCompleto = buscarCompleto;
        $scope.assistencias = [];
        $scope.buscando = false;

        $geo.getEstados().then(res => $scope.estados = res.data);

        function carregarCidades(idEstado) {
            $geo.getCidades(idEstado).then(res => $scope.cidades = res.data);
            buscarByEstado(idEstado);
        }

        function buscarByEstado(idEstado){
            $scope.buscando = true;
            $assistenciaTecnica.getByEstado(idEstado)
                    .then(res => {
                        $scope.assistencias = res.data;
                        $scope.buscando = false;
                    });
        }

        function buscarCompleto(busca){
            $scope.buscando = true;
            $assistenciaTecnica.getByCidade(busca.estado.id, busca.cidade.id)
                .then(res => {
                    $scope.assistencias = res.data
                    $scope.buscando = false;
                });
        }


    }
})();